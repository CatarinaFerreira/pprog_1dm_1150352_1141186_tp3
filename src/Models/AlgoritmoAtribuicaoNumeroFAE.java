package Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AlgoritmoAtribuicaoNumeroFAE extends AlgoritmoAtribuicao implements Serializable {

    public AlgoritmoAtribuicaoNumeroFAE() {
        super();
    }

    public AlgoritmoAtribuicaoNumeroFAE(String descricao) {
        super(descricao);
    }

    /**
     * Permite obter a quantidade de FAE através de uma janela popup.
     *
     * @return quantidadeFAE
     */
    private int obterQuantidadeFAE() {
        String output = (String) JOptionPane.showInputDialog(null,
                "Introduza o nº de FAE aos quais pretende atribuir as candidaturas",
                "Escolha", JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                0);
        if (output == null) {
            throw new IllegalAccessError("Cancelamento por parte do utilizador");
        }
        int qtdFae = Integer.parseInt(output);
        if (qtdFae == 0) {
            throw new IllegalArgumentException("Quantidade de FAE inválida");
        }
        return qtdFae;
    }

    /**
     * Método que, a partir de um algoritmo, atribui um determinado número de
     * FAE por cada candidatura
     *
     * @param listaCandidaturas
     * @param listaFAE
     * @return lista de atribuições
     */
    @Override
    public List<Atribuicao> criarAtribuicoes(List<Candidatura> listaCandidaturas, List<FAE> listaFAE) {
        int numeroFAE = obterQuantidadeFAE(), contFAE = 0, contCand = 0;
        List<Atribuicao> listaAtribuicoes = new ArrayList<>();

        while (validarExistenciaDeCandidaturasPorAtribuir(listaAtribuicoes, listaCandidaturas, numeroFAE)) {
            if (contFAE >= listaFAE.size()) {
                contFAE = 0;
            }

            if (contCand >= listaCandidaturas.size()) {
                contCand = 0;
            }

            Candidatura candidatura = listaCandidaturas.get(contCand);
            FAE fae = listaFAE.get(contFAE);

            if (validarNumeroDeOcorrenciasDeCandidatura(listaAtribuicoes, candidatura) >= numeroFAE) {

                contCand++;

            } else {

                Atribuicao atribuicao = new Atribuicao(candidatura, fae);

                if (listaAtribuicoes.contains(atribuicao) == false) {
                    listaAtribuicoes.add(atribuicao);
                    contFAE++;
                    contCand++;
                } else {
                    contCand++;
                }

            }

        }
        return listaAtribuicoes;
    }

    /**
     * Método que verifica se uma dada candidatura já tem o número necessário de FAE
     * @param listaAtribuicoes
     * @param candidatura
     * @return
     */
    public int validarNumeroDeOcorrenciasDeCandidatura(List<Atribuicao> listaAtribuicoes, Candidatura candidatura) {
        int index = 0;

        if (listaAtribuicoes.isEmpty()) {
            return index;
        }

        for (Atribuicao atribuicao : listaAtribuicoes) {
            if (atribuicao.getCandidatura().equals(candidatura)) {
                index++;
            }
        }

        return index;
    }

    /**
     * Método que verifica se ainda há candidaturas sem o número pretendido de
     * FAE
     *
     * @param listaAtribuicoes
     * @param listaCandidaturas
     * @param numeroFAE
     * @return
     */
    public boolean validarExistenciaDeCandidaturasPorAtribuir(List<Atribuicao> listaAtribuicoes, List<Candidatura> listaCandidaturas, int numeroFAE) {
        if (listaAtribuicoes.isEmpty()) {
            return true;
        }

        for (Candidatura candidatura : listaCandidaturas) {
            if (validarNumeroDeOcorrenciasDeCandidatura(listaAtribuicoes, candidatura) < numeroFAE) {
                return true;
            }
        }
        return false;
    }
}
