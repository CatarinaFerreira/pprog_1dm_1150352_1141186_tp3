package Models;

import Lists.ListaDecisoes;
import java.util.ArrayList;
import java.util.List;

public class Candidatura {

    /**
     * Representante da candidatura.
     */
    private Utilizador representante;
    
    /**
     * Morada da candidatura.
     */
    private String morada;
    
    /**
     * Nome da candidatura.
     */
    private String nome;
    
    /**
     * Telemóvel da candidatura.
     */
    private int telemovel;
    
    /**
     * Lista de FAE registada no sistema.
     */
    private List<FAE> listaFAE;
    
    /**
     * Decisão associada a esta candidatura.
     */
    private final List<Decisao> decisao;
    
    /**
     * Lista de decisões tomadas.
     */
    private ListaDecisoes listaDecisoes;
    
    /**
     * Morada da candidatura por omissão.
     */
    private final String MORADA_POR_OMISSAO = "Sem_Morada";
    
    /**
     * Nome da candidatura por omissão.
     */
    private final String NOME_POR_OMISSAO = "Sem_Nome";
    
    /**
     * Telemóvel da candidatura por omissão.
     */
    private final int TELEMOVEL_POR_OMISSAO = 000000000;

    /**
     * Constrói uma instância de Candidatura com o representante, os dados, a lista de FAE e a lista de decisões 
     * por omissão.
     */
    public Candidatura() {
        this.representante = new Utilizador();
        this.morada = MORADA_POR_OMISSAO;
        this.nome = NOME_POR_OMISSAO;
        this.telemovel = TELEMOVEL_POR_OMISSAO;
        this.listaFAE = new ArrayList<>();
        this.listaDecisoes = new ListaDecisoes();
        this.decisao = new ArrayList<>();
    }
    
    /**
     * Constrói uma instância de Candidatura recebendo, por parâmetro, o representante e os dados e a lista de FAE e 
     * a lista de decisões por omissão.
     * @param representante representante da candidatura.
     * @param morada morada da candidatura.
     * @param nome nome da candidatura.
     * @param telemovel telemóvel da candidatura.
     */
    public Candidatura(Utilizador representante, String morada, String nome, int telemovel) {
        this.representante = representante;
        this.morada = morada;
        this.nome = nome;
        this.telemovel = telemovel;
        this.listaFAE = new ArrayList<>();
        this.listaDecisoes = new ListaDecisoes();
        this.decisao = new ArrayList<>();
    }
    
    /**
     * Constrói uma instância de Candidatura recebendo, por parâmetro, o representante, os dados, a lista de FAE e 
     * a lista de decisões.
     * @param representante utilizador da candidatura.
     * @param morada morada da candidatura.
     * @param nome nome da candidatura.
     * @param telemovel telemóvel da candidatura.
     * @param listaFAE lista de FAE.
     * @param listaDecisoes lista de decisões.
     */
    public Candidatura(Utilizador representante, String morada, String nome, int telemovel, List<FAE> listaFAE, List<Decisao> listaDecisoes) {
        this.representante = representante;
        this.morada = morada;
        this.nome = nome;
        this.telemovel = telemovel;
        this.listaFAE = listaFAE;
        this.listaDecisoes.setListaDecisoes(listaDecisoes);
        this.decisao = new ArrayList<>();
    }
    
    /**
     * Devolve o representante da candidatura.
     * @return representante da candidatura.
     */
    public Utilizador getRepresentante() {
        return this.representante;
    }
    
    /**
     * Devolve a morada da candidatura
     * @return morada da candidatura
     */
    public String getMorada() {
        return this.morada;
    }
    
    /**
     * Altera a morada da candidatura.
     * @param morada a nova morada da candidatura.
     */
    public final void setMorada(String morada) {
        if (morada == null || morada.isEmpty()) {
            throw new IllegalArgumentException("Morada inválida");
        } else {
            this.morada = morada;
        }
    }
    
     /**
     * Devolve o telemóvel da candidatura.
     * @return telemóvel da candidatura
     */
    public int getTelemovel() {
        return this.telemovel;
    }

    /**
     * Altera o número de telemóvel da candidatura.
     * @param telemovel o novo telemóvel da candidatura.
     */
    public final void setTelemovel(int telemovel) {
        if (telemovel > 999999999 || telemovel < 100000000) {
            throw new IllegalArgumentException("Número de telemóvel inválido");
        } else {
            this.telemovel = telemovel;
        }
    }
    
    /**
     * Devolve o nome da candidatura.
     * @return o nome da candidatura
     */
    public String getNome() {
        return this.nome;
    }
    
    /**
     * Altera o nome da candidatura.
     * @param nome novo nome da candidatura.
     */
    public final void setNome(String nome) {
        if (nome == null || nome.isEmpty()) {
            throw new IllegalArgumentException("Nome inválido");
        } else {
            this.nome = nome;
        }
    }
    
    /**
     * Devolve a lista de FAE.
     * @return lista de FAE.
     */
    public List<FAE> getListaFAE() {
        return this.listaFAE;
    }
    
    /**
     * Devolve a lista de decisões.
     * @return lista de decisoes.
     */
    public List<Decisao> getListDecisoes() {
        return this.listaDecisoes.getListaDecisoes();
    }

    /**
     * Modifica o representante da candidatura.
     * @param representante o novo representante da candidatura.
     */
    public void setRepresentante(Utilizador representante) {
        this.representante = representante;
    }
    
    /**
     * Modifica a lista de FAE.
     * @param listaFAE a nova lista de FAE.
     */
    public void setListaFAE(List<FAE> listaFAE) {
        this.listaFAE = listaFAE;
    }
    
    /**
     * Modifica a lista de decisões.
     * @param listaDecisoes a nova lista de decisões.
     */
    public void setListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes.setListaDecisoes(listaDecisoes);
    }
    
    /**
     * Adiciona a decisão tomada à lista de decisões.
     * @param decisao decisão tomada.
     */
    public void addDecisao(Decisao decisao) {
        this.listaDecisoes.addDecisao(decisao);
    }

    /**
     * Valida os dados da candidatura.
     * @return boolean true/false.
     */
    public boolean validaDadosCandidatura() {
        return morada != null && !morada.isEmpty();
    }
    
    /**
     * Cria a decisão e texto descritivo da mesma.
     * @param decisao decisão tomada.
     * @param textoDescritivo texto descritivo criado/adicionado.
     * @return 
     */
    public Decisao createDecisao(boolean decisao, String textoDescritivo) {
        return listaDecisoes.createDecisao(decisao, textoDescritivo);
    }
    
    /**
     * Verifica se a candidatura foi decidida previamente pelo f (FAE)
     *
     * @param f FAE
     * @return boolean
     */
    public boolean decididaPeloFae(FAE f) {
        if (!decisao.isEmpty()) {
            for (Decisao dec : this.decisao) {
                if (dec.getFAE().getUtilizador().getUsername().equals(f.getUtilizador().getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Devolve a descrição textual da candidatura.
     * @return 
     */
    @Override
    public String toString() {
        return this.representante.toString();
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof Candidatura) {
            Candidatura candidatura = (Candidatura) o;
            return this.representante == candidatura.getRepresentante();
        }
        return false;
    }
}