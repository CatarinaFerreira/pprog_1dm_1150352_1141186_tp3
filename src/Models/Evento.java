package Models;

import Lists.ListaAtribuicoes;
import Lists.ListaCandidaturas;
import Lists.ListaOrganizadores;
import java.util.ArrayList;
import java.util.List;

public class Evento {

    /**
     * Título do evento
     */
    private String titulo;

    /**
     * Texto descritivo do evento
     */
    private String textoDescritivo;

    /**
     * Local em que o evento irá decorrer
     */
    private String local;

    /**
     * Período durante o qual irá decorrer o evento
     */
    private Periodo periodo;

    /**
     * Data limite de submissão de candidaturas ao evento, por parte dos
     * participantes
     */
    private Data dataCandidaturas;

    /**
     * Lista de organizadores do evento
     */
    private ListaOrganizadores listaOrganizadores;

    /**
     * Lista de funcionários de apoio ao evento
     */
    private List<FAE> listaFAE;

    /**
     * Lista de candidaturas ao evento
     */
    private ListaCandidaturas listaCandidaturas;

    /**
     * Lista de atribuições FAE-Candidatura
     */
    private ListaAtribuicoes listaAtribuicoes;

    /**
     * Título do evento, por omissão
     */
    private final String TITULO_POR_OMISSAO = "Sem_Titulo";

    /**
     * Texto descritivo do evento, por omissão
     */
    private final String TEXTODESC_POR_OMISSAO = "Sem_Texto_Descritivo";

    /**
     * Local em que o evento irá decorrer, por omissão
     */
    private final String LOCAL_POR_OMISSAO = "Sem_Local";

    /**
     * Periodo, por omissão
     */
    private final Periodo PERIODO_POR_OMISSAO = new Periodo();
    
    /**
     * Data limite de submissão de candidaturas, por omissão
     */
    private final Data DATACANDIDATURAS_POR_OMISSAO = new Data();
    
    /**
     * Inicia um instância da classe Evento, sem argumentos
     */
    public Evento() {
        this.titulo = TITULO_POR_OMISSAO;
        this.textoDescritivo = TEXTODESC_POR_OMISSAO;
        this.local = LOCAL_POR_OMISSAO;
        this.periodo = PERIODO_POR_OMISSAO;
        this.dataCandidaturas = DATACANDIDATURAS_POR_OMISSAO;
        this.listaOrganizadores = new ListaOrganizadores();
        this.listaFAE = new ArrayList<>();
        this.listaCandidaturas = new ListaCandidaturas();
        this.listaAtribuicoes = new ListaAtribuicoes();
    }

    /**
     * Inicia uma instância da classe Evento, enviando o Título, Texto
     * descritivo, Local, Período e Data limite para submissão de candidaturas
     * como argumentos
     *
     * @param titulo
     * @param textoDescritivo
     * @param local
     * @param dataInicio
     * @param dataFim
     * @param dataCandidaturas
     */
    public Evento(String titulo, String textoDescritivo, String local, Data dataInicio, Data dataFim, Data dataCandidaturas) {
        this.titulo = titulo;
        this.textoDescritivo = textoDescritivo;
        this.local = local;
        this.periodo = new Periodo(dataInicio, dataFim);
        this.dataCandidaturas = dataCandidaturas;
        this.listaOrganizadores = new ListaOrganizadores();
        this.listaFAE = new ArrayList<>();
        this.listaCandidaturas = new ListaCandidaturas();
        this.listaAtribuicoes = new ListaAtribuicoes();
    }

    /**
     * Inicia uma instância da classe Evento, enviando o Título, Texto
     * descritivo, Local, Período, Data limite para submissão de candidaturas,
     * Lista de organizadores, Lista de FAE, Lista de Candidaturas e Lista de
     * Atribuições como argumentos
     *
     * @param titulo
     * @param textoDescritivo
     * @param local
     * @param dataInicio
     * @param dataFim
     * @param dataCandidaturas
     * @param listaOrganizadores
     * @param listaFAE
     * @param listaCandidaturas
     * @param listaAtribuicoes
     */
    public Evento(String titulo, String textoDescritivo, String local, Data dataInicio, Data dataFim, Data dataCandidaturas, List<Organizador> listaOrganizadores, List<FAE> listaFAE, List<Candidatura> listaCandidaturas, List<Atribuicao> listaAtribuicoes) {
        this.titulo = titulo;
        this.textoDescritivo = textoDescritivo;
        this.local = local;
        this.periodo = new Periodo(dataInicio, dataFim);
        this.dataCandidaturas = dataCandidaturas;
        this.listaOrganizadores.setListaOrganizadores(listaOrganizadores);
        this.listaFAE = listaFAE;
        this.listaCandidaturas.setListaCandidaturas(listaCandidaturas);
        this.listaAtribuicoes = new ListaAtribuicoes();
    }

    /**
     * Acede ao título do evento
     * @return titulo do evento
     */
    public String getTitulo() {
        return this.titulo;
    }

    /**
     * Acede ao texto descritivo do evento
     * @return textoDescritivo do evento
     */
    public String getTextoDescritivo() {
        return this.textoDescritivo;
    }

    /**
     * Acede ao local do evento
     * @return local do evento
     */
    public String getLocal() {
        return this.local;
    }

    /**
     * Acede ao período do evento
     * @return periodo do evento
     */
    public Periodo getPeriodo() {
        return this.periodo;
    }

    /**
     * Acede à data limite de submissão de candidaturas do evento
     * @return dataCandidaturas do evento
     */
    public Data getDataCandidaturas() {
        return this.dataCandidaturas;
    }

    /**
     * Acede à lista de organizadores do evento
     * @return listaOrganizadores do evento
     */
    public List<Organizador> getListaOrganizadores() {
        return this.listaOrganizadores.getListaOrganizadores();
    }

    /**
     * Acede à lista de FAE do evento
     * @return listaFAE do evento
     */
    public List<FAE> getListaFAE() {
        return this.listaFAE;
    }

    /**
     * Acede à lista de candidaturas do evento
     * @return listaCandidaturas do evento
     */
    public List<Candidatura> getListaCandidaturas() {
        return this.listaCandidaturas.getListaCandidaturas();
    }

    /**
     * Acede à lista de atribuições do evento
     * @return listaAtribuicoes do evento
     */
    public ListaAtribuicoes getListaAtribuicoes() {
        return this.listaAtribuicoes;
    }

    /**
     * Acede à lista de candidaturas atribuídas ao FAE indicado
     * @param utilizador
     * @return candidaturas atribuídas
     */
    public List<Candidatura> getListaCandidaturasFAE(Utilizador utilizador) {
        return this.listaAtribuicoes.getListaCandidaturasFAE(utilizador);
    }

    /**
     * Define o título do evento
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Define o texto descritivo do evento
     * @param textoDescritivo the textoDescritivo to set
     */
    public void setTextoDescritivo(String textoDescritivo) {
        this.textoDescritivo = textoDescritivo;
    }

    /**
     * Define o local do evento
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * Define o período do evento
     * @param periodo the periodo to set
     */
    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    /**
     * Define a data limite de submissão de candidaturas do evento
     * @param dataCandidaturas the dataCandidaturas to set
     */
    public void setDataCandidaturas(Data dataCandidaturas) {
        this.dataCandidaturas = dataCandidaturas;
    }

    /**
     * Define a lista de organizadores do evento
     * @param listaOrganizadores the listaOrganizadores to set
     */
    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores.setListaOrganizadores(listaOrganizadores);
    }

    /**
     * Define a lista de FAE do evento
     * @param listaFAE the listaFAE to set
     */
    public void setListaFAE(List<FAE> listaFAE) {
        this.listaFAE = listaFAE;
    }

    /**
     * Define a lista de candidaturas do evento
     * @param listaCandidaturas the listaCandidaturas to set
     */
    public void setListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas.setListaCandidaturas(listaCandidaturas);
    }
//
//    /**
//     * Define a lista de atribuições do evento
//     * @param listaAtribuicoes
//     */
//    public void setListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
//        this.listaAtribuicoes.setListaAtribuicoes(listaAtribuicoes);
//    }

     /**
     * Devolve a descrição textual do evento.
     *
     * @return descrição textual do .
     */
    @Override
    public String toString() {
        String Texto;
        Texto = String.format("%s;%s;%s;%s%s;\n", this.getTitulo(), this.getTextoDescritivo(), this.getPeriodo().toString(), this.getLocal(), this.getDataCandidaturas());

        for (Organizador org : this.listaOrganizadores.getListaOrganizadores()) {
            Texto += String.format("%s \n", org.toString());
        }
        return Texto;
    }

    /**
     * Verifica se ainda é possível proceder à submissão de candidaturas, ao
     * verificar se a data limite para a submissão de candidaturas já foi
     * ultrapassada
     *
     * @param data
     * @return
     */
    public boolean validarDataParaSubmissao(Data data) {
        return this.dataCandidaturas.isMaior(data);
    }

    /**
     * Verifica se já é possível proceder à atribuição de candidaturas a FAE's
     * para decisão, ao verificar se a data limite para submissão de
     * candidaturas já foi ultrapassada
     *
     * @param data
     * @return
     */
    public boolean validarDataParaAtribuicao(Data data) {
        return data.isMaior(this.dataCandidaturas);
    }

    /**
     * Valida a candidatura indicada, verificando se existe mais alguma submetida pelo mesmo representante
     * @param candidatura
     * @return true se a candidatura for única
     */
    public boolean validaCandidatura(Candidatura candidatura) {
        return this.listaCandidaturas.validaCandidatura(candidatura);
    }

    /**
     * Adiciona a candidatura à lista de candidaturas do evento.
     * @param candidatura candidatura a adicionar.
     */
    public void addCandidatura(Candidatura candidatura) {
        this.listaCandidaturas.registarCandidatura(candidatura);
    }

    /**
     * Verifica se o utilizador já está definido como organizador no evento.
     * @param utilizador
     * @return true se o utilizador não estiver definido como organizador
     */
    public boolean verificaOrganizador(Utilizador utilizador) {
        return this.listaOrganizadores.verificaOrganizador(utilizador);
    }

    /**
     * Verifica se a atribuição indicada já existe na lista de atribuições do evento
     * @param listaAtribuicoes
     * @return true se a atribuição for única
     */
    public boolean validaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        return this.listaAtribuicoes.validaAtribuicoes(listaAtribuicoes);
    }

    /**
     * Adiciona as atribuições contidas na lista à lista de atribuições do evento
     * @param listaAtribuicoes
     */
    public void addListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        this.listaAtribuicoes.addListaAtribuicoes(listaAtribuicoes);
    }

    /**
     * Verifica se a lista de Candidaturas contém alguma candidatura submetida
     * pelo utilizador enviado por parâmetro
     *
     * @param utilizador
     * @return
     */
    public boolean validaEventoParaCandidatura(Utilizador utilizador) {
        return this.listaCandidaturas.validaEventoParaCandidatura(utilizador);
    }

    /**
     * Inicia uma nova instância da classe Candidatura, enviando o utilizador e os dados por parâmetro
     * @param utilizador
     * @param morada
     * @param nome
     * @param telemovel
     * @return
     */
    public Candidatura createCandidatura(Utilizador utilizador, String morada, String nome, int telemovel) {
        return listaCandidaturas.createCandidatura(utilizador, morada, nome, telemovel);
    }
}
