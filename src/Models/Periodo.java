package Models;

public class Periodo {

    // Atributos
    /**
     * Data de início do Evento
     */
    private Data dataInicio;

    /**
     * Data de fim do Evento
     */
    private Data dataFim;

    /**
     * Data de início do Evento, por omissão
     */
    private final Data DATAINICIO_POR_OMISSAO = new Data();

    /**
     * Data de fim do Evento, por omissão
     */
    private final Data DATAFIM_POR_OMISSAO = new Data();

    // Construtores
    /**
     * Inicia uma instância da classe Período, sem parâmetros
     */
    public Periodo() {
        this.dataInicio = DATAINICIO_POR_OMISSAO;
        this.dataFim = DATAFIM_POR_OMISSAO;
    }

    /**
     * Inicia uma instância da classe Período, enviando a data de início e a
     * data de fim como argumentos
     *
     * @param dataInicio
     * @param dataFim
     */
    public Periodo(Data dataInicio, Data dataFim) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }

    // getList
    /**
     * Acede à data de início do evento
     * @return dataInicio
     */
    public Data getDataInicio() {
        return this.dataInicio;
    }

    /**
     * Acede à data de fim do evento
     * @return dataFim
     */
    public Data getDataFim() {
        return this.dataFim;
    }

    // setList
    /**
     * Define a data de início do evento
     * @param dataInicio 
     */
    public void setDataInicio(Data dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * Define a data de fim do evento
     * @param dataFim 
     */
    public void setDataFim(Data dataFim) {
        this.dataFim = dataFim;
    }

    // Métodos
    /**
     * Descrição textual da classe Período
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
}
