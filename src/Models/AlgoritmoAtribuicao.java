
package Models;

import UI.AlgoritmoAtribuicaoUI;
import java.util.List;

public abstract class AlgoritmoAtribuicao implements AlgoritmoAtribuicaoUI{
    
    private String descricao;
    
    public AlgoritmoAtribuicao() {
        this.descricao = "Algoritmo_Atribuição_Candidaturas_Sem_Descrição";
    }
    
    public AlgoritmoAtribuicao(String descricao) {
        this.descricao = descricao;
    }
    
    @Override
    public abstract List<Atribuicao> criarAtribuicoes(List<Candidatura> listaCandidaturas, List<FAE> listaFAE);
    
    @Override
    public void setDescricao(String descricao) {
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }
    
    @Override
    public String toString() {
        return this.descricao;
    }
}
