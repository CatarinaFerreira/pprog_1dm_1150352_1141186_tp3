package Models;

import java.io.Serializable;
import java.util.Objects;

public class FAE implements Serializable{

    /**
     * Utilizador definido como FAE
     */
    private Utilizador utilizador;
    
    /**
     * Experiência do FAE.
     */
    private int experiencia;

    /**
     * A carga de trabalho do fae
     */
    private int carga;
    
    /**
     * Utilizador definido como FAE, por omissão
     */
    private final Utilizador UTILIZADOR_POR_OMISSAO = new Utilizador();
    
    /**
     * Experiência inicial do FAE.
     */
    private final static int EXPERIENCIA_INICIAL = 0;

    /**
     * Inicia uma instância da classe FAE, sem argumentos
     */
    public FAE() {
        this.utilizador = UTILIZADOR_POR_OMISSAO;
        this.experiencia = EXPERIENCIA_INICIAL;
    }

    /**
     * Inicia uma instância da classe FAE, enviando o utilizador como argumento
     * @param utilizador utilizador recebido.
     */
    public FAE(Utilizador utilizador) {
        setUtilizador(utilizador);
        this.experiencia = EXPERIENCIA_INICIAL;
    }

    /**
     * Método que devolve o utilizador.
     * @return utilizador.
     */
    public Utilizador getUtilizador() {
        return this.utilizador;
    }

    /**
     * Devolve a experiência do FAE.
     *
     * @return
     */
    public int getExperiencia() {
        return this.experiencia;
    }
    
    /**
     * Método que permite modificar o utilizador, recebendo o novo como parâmetro.
     * @param utilizador novo utilizador.
     */
    public void setUtilizador(Utilizador utilizador) {
        if (utilizador == null) {
            throw new IllegalArgumentException("Utilizador não existe");
        }
        this.utilizador = utilizador;
    }
    
    /**
     * Devolve a carga de trabalho do FAE.
     *
     * @return carga - a carga de trabalho do FAE.
     */
    public int getCarga() {
        return carga;
    }

    /**
     * Incrementa a carga ao FAE.
     */
    public void adicionaCarga() {
        this.carga++;
    }
    
    /**
     * Decrementa a carga ao FAE.
     */
    public void retiraCarga() {
        this.carga--;
    }

    /**
     * Método que valida o FAE.
     * @return boolean com resultado
     */
    public boolean valida(){
        return utilizador.valida();
    }

    
    /**
     * Devolve a descrição textual do FAE.
     * @return 
     */   
    @Override
    public String toString() {
        return this.utilizador != null ? this.utilizador.toString() : null;
    }  
    
    /**
     * Método que compara dois FAE. 
     * @param o FAE a comparar.
     * @return boolean com resultado da comparação
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof FAE) {
            FAE fae = (FAE) o;
            return this.utilizador == fae.getUtilizador();
        }
        return false;
    }
}
