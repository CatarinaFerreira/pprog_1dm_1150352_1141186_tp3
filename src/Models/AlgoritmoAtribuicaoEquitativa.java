package Models;

import java.util.ArrayList;
import java.util.List;

public class AlgoritmoAtribuicaoEquitativa extends AlgoritmoAtribuicao {

    public AlgoritmoAtribuicaoEquitativa() {
        super();
    }

    public AlgoritmoAtribuicaoEquitativa(String descricao) {
        super(descricao);
    }

    @Override
    public List<Atribuicao> criarAtribuicoes(List<Candidatura> listaCandidaturas, List<FAE> listaFAE) {
        int tamanhoNovaLista = lcm(listaFAE.size(), listaCandidaturas.size());
        List<Atribuicao> listaAtribuicoes = new ArrayList<>();
        
        int cont = 0, contFAE = 0, contCand = 0;
        
        while(cont < tamanhoNovaLista) {
            if(contFAE >= listaFAE.size()) {
                contFAE = 0;
            }
            
            if(contCand >= listaCandidaturas.size()) {
                contCand = 0;
            }
            
            Atribuicao atribuicao = new Atribuicao(listaCandidaturas.get(contCand), listaFAE.get(contFAE));
            
            if(listaAtribuicoes.contains(atribuicao) == false) {
                listaAtribuicoes.add(atribuicao);
            }
            
            contFAE++;
            contCand++;
            cont++;
        }
        
        return listaAtribuicoes;
    }
    
    /**
     * cálculo do maior divisor comum entre a e b
     * @param a
     * @param b
     * @return 
     */
    public static int gcd(int a, int b) {
        while (b > 0) {
            int temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }

    /**
     * cálculo do menor múltiplo comum
     * @param a
     * @param b
     * @return 
     */
    public static int lcm(int a, int b) {
        return a * (b / gcd(a, b));
    }
}
