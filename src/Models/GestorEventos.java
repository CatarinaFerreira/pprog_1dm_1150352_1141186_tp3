package Models;

import java.io.Serializable;

public class GestorEventos implements Serializable {

    /**
     * Utilizador definido como GestorEventos
     */
    private Utilizador utilizador;

    /**
     * Utilizador definido como GestorEventos, por omissão
     */
    private final Utilizador UTILIZADOR_POR_OMISSAO = new Utilizador();

    private static final String ROOT_ELEMENT_NAME = "gestor_eventos"; // ??????
    
    /**
     * Inicia uma instância da classe GestorEventos, sem parâmetros.
     */
    public GestorEventos() {
        this.utilizador = UTILIZADOR_POR_OMISSAO;
    }

    /**
     * Inicia uma instância da classe GestorEventos, enviando o utilizador como argumento
     * @param utilizador 
     */
    public GestorEventos(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * Método que devolve o utilizador.
     * @return utilizador.
     */
    public Utilizador getUtilizador() {
        return this.utilizador;
    }

    /**
     * Método que permite modificar o utilizador, recebendo o novo como parâmetro.
     * @param utilizador novo utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * Método que valida o GestorEventos.
     * @return boolean com o resultado obtido.
     */
    public boolean valida(){
        return utilizador.valida();
    }
   
    /**
     * Método que compara dois gestores de eventos.
     * @param obj
     * @return booelan com resultado da comparação.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof GestorEventos) {
            GestorEventos that = (GestorEventos) obj;
            return that.utilizador.equals(this.utilizador);
        }
        return false;
    }

    /**
     * Descrição textual da classe GestorEventos
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
}
