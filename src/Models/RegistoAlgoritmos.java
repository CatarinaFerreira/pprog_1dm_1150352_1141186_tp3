package Models;

import UI.AlgoritmoAtribuicaoUI;
import java.util.ArrayList;
import java.util.List;

public class RegistoAlgoritmos {
    
    /**
    * Lista que vai receber os Algoritmos de Atribuição.
    */
    private final List<AlgoritmoAtribuicaoUI> listaAlgoritmos;

    /**
    * Cria uma lista de Algoritmos.
    */
    public RegistoAlgoritmos() {
        listaAlgoritmos = new ArrayList<>();
    }

    /**
    * Metodo que retorna a lista de mecanismos
    * @return listaMecanismos 
    */
    public List<AlgoritmoAtribuicaoUI> getMecanismos() {
        return listaAlgoritmos;
    }

    /**
     * Metodo que adiciona um mecanismo a lista de mecanismos; 
     * @param aa algoritmo de atribuição.
    */
    public void addAlgoritmo(AlgoritmoAtribuicaoUI aa) {
        listaAlgoritmos.add(aa);
    }   
}

