
package Models;

import java.io.Serializable;

public class Atribuicao implements Serializable {
    
    /**
     * Candidatura selecionada pelo algoritmo
     */
    private Candidatura candidatura;
    
    /**
     * FAE selecionado pelo algoritmo
     */
    private FAE fae;
    
    /**
     * Candidatura selecionada pelo algoritmo, por omissão
     */
    private final Candidatura CANDIDATURA_POR_OMISSAO = new Candidatura();
    
    /**
     * FAE selecionado pelo algoritmo, por omissão
     */
    private final FAE FAE_POR_OMISSAO = new FAE();
    
    // Construtores
    /**
     * Inicia uma instância da classe Atribuicao, sem argumentos
     */
    public Atribuicao() {
        this.candidatura = CANDIDATURA_POR_OMISSAO;
        this.fae = FAE_POR_OMISSAO;
    }
    
    /**
     * Inicia uma instância da classe Atribuicao, enviando a candidatura e o FAE como argumentos
     * @param candidatura
     * @param fae 
     */
    public Atribuicao(Candidatura candidatura, FAE fae) {
        this.candidatura = candidatura;
        this.fae = fae;
    }
    
    // getList
    /**
     * Acede à candidatura da atribuição
     * @return candidatura
     */
    public Candidatura getCandidatura() {
        return this.candidatura;
    }
    
    /**
     * Acede ao FAE da atribuição
     * @return fae
     */
    public FAE getFAE() {
        return this.fae;
    }
    
    // setList
    /**
     * Define o atributo candidatura
     * @param candidatura 
     */
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
    
    /**
     * Define o atributo FAE
     * @param fae 
     */
    public void setFAE(FAE fae) {
        this.fae = fae;
    }
    
    // Métodos
    /**
     * Devolve a descrição textual da classe Atribuicao.
     * @return 
     */
    @Override
    public String toString() {
        return "FAE: " + this.fae.toString() + " - Candidatura: " + this.candidatura.toString();
    }
    
    /**
     * Compara os atributos das duas instâncias
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof Atribuicao) {
            Atribuicao atribuicao = (Atribuicao) o;
            return atribuicao.getCandidatura().equals(this.candidatura) && atribuicao.getFAE().equals(this.fae);
        }
        return false;
    }
}
