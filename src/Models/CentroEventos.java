package Models;

import Lists.ListaAlgoritmosAtribuicao;
import Lists.ListaCandidaturas;
import Lists.ListaEventos;
import Lists.ListaGestores;
import Lists.ListaUtilizadores;
import java.util.List;

public class CentroEventos {

    // Atributos
    /**
     * Lista de eventos registados no Centro de Eventos
     */
    private ListaEventos listaEventos;

    /**
     * Lista de Utilizadores registados no Centro de Eventos
     */
    private ListaUtilizadores listaUtilizadores;

    /**
     * Lista de gestores de eventos registados no Centro de Eventos
     */
    private ListaGestores listaGestores;
    
    /**
     * Lista de Algoritmos de Atribuição de Candidaturas registados no Centro de Eventos
     */
    private ListaAlgoritmosAtribuicao listaAlgoritmos;
    
    private ListaCandidaturas lc;

    // Construtores
    /**
     * Inicia uma instância da classe CentroEventos, sem argumentos
     */
    public CentroEventos() {
        this.listaEventos = new ListaEventos();
        this.listaUtilizadores = new ListaUtilizadores();
        this.listaGestores = new ListaGestores();
        this.listaAlgoritmos = new ListaAlgoritmosAtribuicao(); // VAI TER DE SER ALTERADO PARA QUE PROGRAMA FUNCION - CATARINA
    }

    /**
     * Inicia uma instância da classe CentroEventos, enviando a lista de eventos, lista de utilizadores, lista de gestores e lista de algoritmos como parâmetros
     * @param listaEventos
     * @param listaUtilizadores
     * @param listaGestores
     * @param listaAlgoritmos
     */
    public CentroEventos(List<Evento> listaEventos, List<Utilizador> listaUtilizadores, List<GestorEventos> listaGestores, List<AlgoritmoAtribuicao> listaAlgoritmos) {
        this.listaEventos = new ListaEventos(listaEventos);
        this.listaUtilizadores = new ListaUtilizadores(listaUtilizadores);
        this.listaGestores = new ListaGestores(listaGestores);
        this.listaAlgoritmos = new ListaAlgoritmosAtribuicao(listaAlgoritmos);
        
    }

    // getList
    /**
     * Acede à lista de eventos do centro de eventos
     * @return lista de eventos
     */
    public List<Evento> getListaEventos() {
        return this.listaEventos.getListaEventos();
    }

    /**
     * Acede à lista de utilizadores do centro de eventos
     * @return lista de utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores.getListaUtilizadores();
    }

    /**
     * Acede à lista de gestores do centro de eventos
     * @return lista de gestores
     */
    public List<GestorEventos> getListaGestores() {
        return this.listaGestores.getListaGestores();
    }
    
    public ListaCandidaturas getListaCandidaturas() {
        return this.lc;
    }

    /**
     * Acede à lista de algoritmos de atribuição do centro de eventos
     * @return lista de algoritmos de atribuição
     */
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        return this.listaAlgoritmos.getListaAlgoritmos();
    }
    
    /**
     * Adiciona o algoritmo à lista de algoritmos.
     *
     * @param atribuicao algoritmo
     */
        public void addAlgoritmo(AlgoritmoAtribuicao atribuicao) {
        this.listaAlgoritmos.addAlgoritmo(atribuicao);
    }
    
    /**
     * Acede à lista de eventos que se encontram dentro do período de submissão de candidaturas
     * @param data
     * @return lista de eventos em período de submissão
     */
    public List<Evento> getListaEventosSubmissao(Data data) {
        return this.listaEventos.getListaEventosSubmissao(data);
    }

    /**
     * Acede à lista de eventos nos quais o utilizador indicado está definido como FAE
     * @param utilizador
     * @return lista de eventos onde é fae
     */
    public List<Evento> getListaEventosFAE(Utilizador utilizador) {
        return this.listaEventos.getListaEventosFAE(utilizador);
    }
    
    /**
     * Acede ao utilizador associado ao email indicado
     * @param email
     * @return utilizador do email
     */
    public Utilizador getUtilizadorEmail(String email) {
        return this.listaUtilizadores.getUtilizadorEmail(email);
    }
    
    /**
     * Acede à lista de eventos nos quais o utilizador indicado está definido como Organizador e que estejam em período de atribuição de candidaturas
     * @param utilizador
     * @param data
     * @return lista de eventos onde é organizador e que estejam em período de atribuição
     */
    public List<Evento> getListaEventosOrganizadorAtribuicao(Utilizador utilizador, Data data) {
        return this.listaEventos.getListaEventosOrganizadorAtribuicao(utilizador, data);
    }

    /**
     * Devolve o utilizador a utilizar o sistema.
     * @return utilizador
     */
    public Utilizador getLoggedInUser() {
        return listaUtilizadores.getListaUtilizadores().get(0); // Alterar (quando for corrigido tem que se fazer alterações nos controllers, portanto ficar atentos!)
    }
    
    /**
     * Define a lista de eventos
     * @param listaEventos
     */
    public void setListaEventos(List<Evento> listaEventos) {
        this.listaEventos.setListaEventos(listaEventos);
    }

    /**
     * Define a lista de utilizadores
     * @param listaUtilizadores
     */
    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores.setListaUtilizadores(listaUtilizadores);
    }

    /**
     * Define a lista de gestores
     * @param listaGestores
     */
    public void setListaGestores(List<GestorEventos> listaGestores) {
        this.listaGestores.setListaGestores(listaGestores);
    }
}
