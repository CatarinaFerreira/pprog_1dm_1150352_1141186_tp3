package Models;

public class Organizador {

    /**
     * Utilizador definido como Organizador
     */
    private Utilizador utilizador;

    /**
     * Utilizador definido como Organizador, por omissão
     */
    private final Utilizador UTILIZADOR_POR_OMISSAO = new Utilizador();

    /**
     * Constrói uma instância de Organizador com o utilizador por omissão.
     */
    public Organizador() {
        this.utilizador = UTILIZADOR_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Organizador recebendo, por parâmetro, o utilizador.
     * @param utilizador o utilizador.
     */
    public Organizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * Devolve o utilizador.
     * @return utilizador.
     */
    public Utilizador getUtilizador() {
        return this.utilizador;
    }

    /**
     * Modifica o utilizador/organizador.
     * @param utilizador o novo utilizador.
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }
    
    /**
     * Devolve a descrição textual do Organizador.
     * @return 
     */
    @Override
    public String toString() {
        return this.utilizador.toString();
    }
    
    /**
     * Verifica se o organizador em questão é um utilizador.
     *
     * @param u Utilizador.
     * @return
     */
    public boolean isUtilizador(Utilizador u) {
        if (this.utilizador != null) {
            return this.utilizador.equals(u);
        }
        return false;
    }
    
    /**
     * Verifica se existem dois organizadores iguais.
     * @param other 
     * @return 
     */
    @Override
    public boolean equals(Object other) {
        boolean result = other != null && getClass().equals(other.getClass());
        if (result) {
            Organizador o = getClass().cast(other);
            result = (this == other) || (utilizador.equals(o.utilizador));
        }
        return result;
    }
}
