package Models;

import java.io.Serializable;

public class Decisao implements Serializable {

    /**
     * Funcionário (FAE) que tomou uma decisâo sobre as candidaturas.
     */
    private FAE fae;
    
    /**
     * Decisão (string) tomada sobre candidatura
     */
    private String decisao;
    
    /**
     * Decisão tomada sobre a candidatura
     */
    private boolean decisaoBoolean;
    
    /**
     * Texto descritivo sobre decisão
     */
    private String textoDescritivo;

    /**
     * Decisão (string) sobre a candidatura, por omissão
     */
    private final String DECISAO_POR_OMISSAO = "Rejeitada";
    
    /**
     * Decisão sobre a candidatura, por omissão
     */
    private final boolean DECISAOBOOLEAN_POR_OMISSAO = false;
    
    /**
     * Texto descritivo sobre decisão, por omissão
     */
    private final String TEXTODESCRITIVO_POR_OMISSAO = "Sem_Texto_Descritivo";

    // Construtores
    /**
     * Inicia uma instância da classe Decisao, sem argumentos
     */
    public Decisao() {
        this.decisaoBoolean = DECISAOBOOLEAN_POR_OMISSAO;
        this.textoDescritivo = TEXTODESCRITIVO_POR_OMISSAO;
        setDecisao(decisaoBoolean);
    }

    /**
     * Inicia uma instância da classe Decisao, enviando a decisaoBoolean e o textoDescritivo como argumentos
     * @param decisao
     * @param textoDescritivo 
     */
    public Decisao(boolean decisao, String textoDescritivo) {
        this.decisaoBoolean = decisao;
        this.textoDescritivo = textoDescritivo;
        setDecisao(decisaoBoolean);
    }

    // getList
    /**
     * Acede ao texto de decisão
     * @return decisão
     */
    public String getDecisao() {
        return this.decisao;
    }
    
    /**
     * Acede à decisão
     * @return decisãoBoolean
     */
    public boolean getDecisaoBoolean() {
        return this.decisaoBoolean;
    }

    /**
     * Acede ao texto descritivo da decisão
     * @return texto descritivo
     */
    public String getTextoDescritivo() {
        return this.textoDescritivo;
    }
    
    /**
     * Permite obter o FAE que efetuou a decisão.
     * @return fae FAE que tomou a decisão.
     */
    public FAE getFAE() {
        return fae;
    }

    // setList
    /**
     * Define o texto de decisão
     * @param decisaoBoolean 
     */
    public void setDecisao(boolean decisaoBoolean) {
        if(decisaoBoolean) {
            this.decisao = "Aprovada";
        } else {
            this.decisao = DECISAO_POR_OMISSAO;
        }
    }
    
    /**
     * Define a decisão
     * @param decisaoBoolean 
     */
    public void setDecisaoBoolean(boolean decisaoBoolean) {
        this.decisaoBoolean = decisaoBoolean;
    }

    /**
     * Define o texto descritivo
     * @param textoDescritivo 
     */
    public void setTextoDescritivo(String textoDescritivo) {
        this.textoDescritivo = textoDescritivo;
    }

    // Métodos
    /**
     * Devolve a descrição textual da decisão.
     * @return 
     */
    @Override
    public String toString() {
        return this.decisao;
    }

    /**
     * Verifica se o texto descritivo não é null nem está vazio
     * @return false se o texto descritivo for null ou estiver vazio, true caso não seja null e tenha conteúdo
     */
    public boolean validaDecisao() {
        return textoDescritivo != null && !textoDescritivo.isEmpty();
    }
}
