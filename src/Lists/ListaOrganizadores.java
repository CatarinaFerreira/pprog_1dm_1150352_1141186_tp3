
package Lists;

import Models.Decisao;
import Models.Organizador;
import Models.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class ListaOrganizadores {
    
    // Atributos
    /**
     * 
     */
    private List<Organizador> listaOrganizadores;
    
    // Construtores
    /**
     * 
     */
    public ListaOrganizadores() {
        this.listaOrganizadores = new ArrayList<>();
    }
    
    /**
     * 
     * @param listaOrganizadores
     */
    public ListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<Organizador> getListaOrganizadores() {
        return this.listaOrganizadores;
    }
    
    // setList
    /**
     * 
     * @param listaOrganizadores
     */
    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
    
    /**
     * 
     * @param utilizador
     * @return 
     */
    public boolean verificaOrganizador(Utilizador utilizador) {
        for(Organizador organizador : this.listaOrganizadores) {
            if(organizador.getUtilizador().equals(utilizador)) {
                return true;
            }
        }
        return false;
    }
}
