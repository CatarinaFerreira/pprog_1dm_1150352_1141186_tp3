
package Lists;

import Models.Candidatura;
import Models.Decisao;
import Models.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class ListaCandidaturas {
    
    // Atributos
    /**
     * Lista de Candidaturas do Evento
     */
    private List<Candidatura> listaCandidaturas;
    
    // Construtores
    /**
     * Cria uma instância da classe ListaCandidaturas, com todos os atributos definidos com os valores por omissão
     */
    public ListaCandidaturas() {
        this.listaCandidaturas = new ArrayList<>();
    }
    
    /**
     * Cria uma instância da classe ListaCandidaturas, recebendo por parâmetro, a lista de Candidaturas
     * @param listaCandidaturas 
     */
    public ListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }
    
    // getList
    /**
     * Acede à lista de Candidaturas
     * @return 
     */
    public List<Candidatura> getListaCandidaturas() {
        return this.listaCandidaturas;
    }
    
    // setList
    /**
     * 
     * @param listaCandidaturas 
     */
    public void setListaCandidaturas(List<Candidatura> listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
    
    /**
     *
     * @param candidatura
     * @return
     */
    public boolean validaCandidatura(Candidatura candidatura) {
        for (Candidatura candidaturaRegistada : this.listaCandidaturas) {
            if (candidaturaRegistada.getRepresentante().equals(candidatura.getRepresentante())) {
                return false;
            }
        }
        return true;
    }
    
    public boolean registarCandidatura(Candidatura candidatura) {
        return listaCandidaturas.contains(candidatura)
                ? false
                : listaCandidaturas.add(candidatura);
    }
    
    /**
     * Verifica se a lista de Candidaturas contém alguma candidatura submetida pelo utilizador enviado por parâmetro
     * @param utilizador
     * @return 
     */
    public boolean validaEventoParaCandidatura(Utilizador utilizador) {
        for(Candidatura candidatura : listaCandidaturas) {
            if(candidatura.getRepresentante().equals(utilizador)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 
     * @param representante representante da candidatura.
     * @param morada
     * @param nome
     * @param telemovel
     * @return 
     */
    public Candidatura createCandidatura(Utilizador representante, String morada, String nome, int telemovel) {
        return new Candidatura(representante, morada, nome, telemovel);
    }
    
    /**
     * Cria uma nova candidatura
     *
     * @return Candidatura
     */
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }
    
    /**
     * Regista a decisão de numa determinada candidatura.
     * @param c candidatura.
     * @param d decisão.
     */
    public void registarDecisao(Candidatura c, Decisao d) {
        listaCandidaturas.get(listaCandidaturas.indexOf(c)).addDecisao(d);
    }
}
