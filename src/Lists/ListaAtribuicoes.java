
package Lists;

import Models.Atribuicao;
import Models.Candidatura;
import Models.FAE;
import Models.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class ListaAtribuicoes {

    /**
     * Lista de atribuições.
     */
    private final List<Atribuicao> listaAtribuicoes;
    
    // Construtores
    /**
     * 
     */
    public ListaAtribuicoes() {
        this.listaAtribuicoes = new ArrayList<>();
    }
    
    /**
     * 
     * @param listaAtribuicoes 
     */
    public ListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        this.listaAtribuicoes = listaAtribuicoes;
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<Atribuicao> getListaAtribuicoes() {
        return this.listaAtribuicoes;
    }
    
    /**
     * 
     * @param utilizador
     * @return 
     */
    public List<Candidatura> getListaCandidaturasFAE(Utilizador utilizador) {
        List<Candidatura> listaCandidaturas = new ArrayList<>();
        for(Atribuicao atribuicao : this.listaAtribuicoes) {
            if(atribuicao.getFAE().getUtilizador().equals(utilizador)) {
                listaCandidaturas.add(atribuicao.getCandidatura());
            }
        }
        return listaCandidaturas;
    }

    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
    
    /**
     * 
     * @param listaAtribuicoes
     * @return 
     */
    public boolean validaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        for(Atribuicao atribuicao : listaAtribuicoes) {
            if(this.listaAtribuicoes.contains(atribuicao) == false) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @param listaAtribuicoes 
     */
    public void addListaAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        for(Atribuicao atribuicao : listaAtribuicoes) {
            if(this.listaAtribuicoes.contains(atribuicao) == false) {
                this.listaAtribuicoes.add(atribuicao);
            }
        }
    }
    
    /**
     * Método que regista as atribuições na lista de atribuições.
     * 
     * @param la lista de atribuições.
     * @return 
     */
    
    public boolean registarAtribuicoes(List<Atribuicao> la) {
        return listaAtribuicoes.addAll(la);
    }
    
    /**
     * Verifica se a candidatura já foi atribuida ao FAE.
     * @param f FAE.
     * @param c Candidatura.
     * @return boolean
     */
    public boolean hasAtribuicao(FAE f, Candidatura c) {
        for (Atribuicao a : listaAtribuicoes) {
            if (a.getCandidatura().equals(c)) {
                if (f.getUtilizador().getUsername().equals(a.getFAE().getUtilizador().getUsername())) {
                    return true;
                }
            }
        }
        return false;
    }
}


