
package Lists;

import Models.GestorEventos;
import java.util.ArrayList;
import java.util.List;

public class ListaGestores {
    
    // Atributos
    /**
     * 
     */
    private List<GestorEventos> listaGestores;
    
    // Construtores
    /**
     * 
     */
    public ListaGestores() {
        this.listaGestores = new ArrayList<GestorEventos>();
    }
    
    /**
     * 
     * @param listaGestores 
     */
    public ListaGestores(List<GestorEventos> listaGestores) {
        this.listaGestores = listaGestores;
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<GestorEventos> getListaGestores() {
        return this.listaGestores;
    }
    
    // setList
    /**
     * 
     * @param listaGestores 
     */
    public void setListaGestores(List<GestorEventos> listaGestores) {
        this.listaGestores = listaGestores;
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
}
