
package Lists;

import Models.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class ListaUtilizadores {
    
    /**
     * Lista que irá armazenar os utilizadores.
     */
    private List<Utilizador> listaUtilizadores;
    
    /**
     * Método que cria a lista de utilizadores.
    */
    public ListaUtilizadores() {
        this.listaUtilizadores = new ArrayList<>();
    }
    
    public ListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
    
    /**
     * Método que retorna a lista de utilizadores.
     * @return lista de utilizadores.
    */
    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores;
    }
    
    /**
     * Permite obter um determinado utilizador através do username.
     * @param username username de um utilizador
     * @return u utilizador que encontrou no sistema
     */
    public Utilizador getUtilizador(String username) {
        for (Utilizador u : listaUtilizadores) {
            if (u.getUsername().equals(username)) {
                return u;
            }
        }
        throw new IllegalArgumentException("Utilizador não está registado no sistema");
    }
    
    /**
     * Método que permite modificar a lista de utilizadores.
     * @param listaUtilizadores nova lista de utilizadores.
     */
    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }
   
    /**
     * 
     * @param email
     * @return 
     */
    public Utilizador getUtilizadorEmail(String email) {
        for(Utilizador utilizador : this.listaUtilizadores) {
            if(utilizador.getEmail().equals(email)) {
                return utilizador;
            }
        }
        return null; // ??????????????
    }
    
    /**
     * Método que cria um novo utilizador.
     * @return new Utilizador()
    */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }
    
    /**
     * Método que adiciona um utilizador à lista de utilizadores depois de este ter sido validado;
     * @param ut utilizador a adicionar.
     * @return 
    */
    public boolean registarUtilizador(Utilizador ut) {
        if (valida(ut)) {
            return addUtilizador(ut);
        }
        return false;
    }
    
    /**
     * Método que valida e verifica se o utilizador já existe na lista de utilizadores.
     * @param utilizador utilizador a validar.
     * @return utilizador.valida() && !listaUtilizadores.contains(utilizador);
    */
    private boolean valida(Utilizador utilizador) {
        return utilizador.valida() && !listaUtilizadores.contains(utilizador);
    }
    
    /**
     * Método que adiciona um utilizador à lista de utilizadores.
     * @param utilizador utilizador a adicionar.
     * @return listaUtilizadores.add(utilizador)
    */
    private boolean addUtilizador(Utilizador utilizador) {
        return listaUtilizadores.add(utilizador);
    } 
    
    /**
     * Método que compara dois objetos da lista de utilizadores.
     * @param obj objeto a comparar.
     * @return 
    */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ListaUtilizadores) {
            ListaUtilizadores that = (ListaUtilizadores) obj;
            return that.listaUtilizadores.equals(this.listaUtilizadores);
        }
        return false;
    }
}
