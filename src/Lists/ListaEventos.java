
package Lists;

import Models.Data;
import Models.Evento;
import Models.FAE;
import Models.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class ListaEventos {
    
    // Atributos
    /**
     * Lista de Eventos registados no Centro de Eventos
     */
    private List<Evento> listaEventos;
    
    // Construtores
    /**
     * 
     */
    public ListaEventos() {
        this.listaEventos = new ArrayList<>();
    }
    
    /**
     * 
     * @param listaEventos 
     */
    public ListaEventos(List<Evento> listaEventos) {
        this.listaEventos = listaEventos;
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<Evento> getListaEventos() {
        return this.listaEventos;
    }
    
    public List<Evento> getListaEventosFAE(Utilizador utilizador) {
        List<Evento> listaEventos = new ArrayList<>();
        for(Evento evento : this.getListaEventos()) {
            for(FAE fae : evento.getListaFAE()) {
                if(fae.getUtilizador().equals(utilizador)) {
                    listaEventos.add(evento);
                }
            }
        }
        return listaEventos;
    }
    
    public List<Evento> getListaEventosSubmissao(Data data) {
        List<Evento> listaEventos = new ArrayList<>();
        for (Evento evento : this.getListaEventos()) {
            if (evento.validarDataParaSubmissao(data)) {
                listaEventos.add(evento);
            }
        }
        return listaEventos;
    }
    
    
    public List<Evento> getListaEventosOrganizadorAtribuicao(Utilizador utilizador, Data data) {
        List<Evento> listaEventos = new ArrayList<>();
        for (Evento evento : this.getListaEventos()) {
            if (evento.validarDataParaAtribuicao(data) && evento.verificaOrganizador(utilizador)) {
                listaEventos.add(evento);
            }
        }
        return listaEventos;
    }
    
    // setList
    /**
     * 
     * @param listaEventos 
     */
    public void setListaEventos(List<Evento> listaEventos) {
        this.listaEventos = listaEventos;
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
}

