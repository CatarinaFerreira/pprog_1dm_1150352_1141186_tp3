
package Lists;

import Models.AlgoritmoAtribuicao;
import Models.AlgoritmoAtribuicaoEquitativa;
import Models.AlgoritmoAtribuicaoExperiencia;
import Models.AlgoritmoAtribuicaoNumeroFAE;
import java.util.ArrayList;
import java.util.List;

public class ListaAlgoritmosAtribuicao {
    
    // Atributos
    /**
     * 
     */
    private List<AlgoritmoAtribuicao> listaAlgoritmos;
    
    // Construtores
    /**
     * 
     */
    public ListaAlgoritmosAtribuicao() {
        this.listaAlgoritmos = new ArrayList<>();
        addAlgoritmosPreExistentes();
    }
    
    /**
     * 
     * @param listaAlgoritmos 
     */
    public ListaAlgoritmosAtribuicao(List<AlgoritmoAtribuicao> listaAlgoritmos) {
        this.listaAlgoritmos = listaAlgoritmos;
        addAlgoritmosPreExistentes();
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        return this.listaAlgoritmos;
    }
    
    // setList
    /**
     * 
     * @param listaAlgoritmos 
     */
    public void setListaAtribuicoes(List<AlgoritmoAtribuicao> listaAlgoritmos) {
        this.listaAlgoritmos = listaAlgoritmos;
        addAlgoritmosPreExistentes();
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "toString de ListaAlgoritmosAtribuicao";
    }
    
    /**
     * Adiciona os algoritmos já definidos no sistema a uma nova lista que seja criada
     */
    public final void addAlgoritmosPreExistentes() {
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoEquitativa("Algoritmo de Atribuição Equitativa de Candidaturas"));
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoExperiencia("Algoritmo de Atribuição de Candidaturas por Experiência"));
        this.listaAlgoritmos.add(new AlgoritmoAtribuicaoNumeroFAE("Algoritmo de Atribuição de Candidaturas por Número de FAE"));
    }
    
    /**
     * Adiciona o algoritmo enviado como argumento à lista de algoritmos de atribuição de FAE's
     * @param algoritmo 
     */
    public void addAlgoritmo(AlgoritmoAtribuicao algoritmo) {
        this.listaAlgoritmos.add(algoritmo);
    }
}
