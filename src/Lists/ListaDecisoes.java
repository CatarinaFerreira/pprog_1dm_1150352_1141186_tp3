
package Lists;

import Models.Decisao;
import java.util.ArrayList;
import java.util.List;

public class ListaDecisoes {
    
    // Atributos
    /**
     * 
     */
    private List<Decisao> listaDecisoes;
    
    // Construtores
    /**
     * 
     */
    public ListaDecisoes() {
        this.listaDecisoes = new ArrayList<>();
    }
    
    /**
     * 
     * @param listaDecisoes
     */
    public ListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }
    
    // getList
    /**
     * 
     * @return 
     */
    public List<Decisao> getListaDecisoes() {
        return this.listaDecisoes;
    }
    
    // setList
    /**
     * 
     * @param listaDecisoes
     */
    public void setListaDecisoes(List<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }
    
    // Métodos
    /**
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "x";
    }
    
    /**
     * 
     * @param decisao 
     */
    public void addDecisao(Decisao decisao) {
        this.listaDecisoes.add(decisao);
    }
    
    /**
     * 
     * @param decisao
     * @param textoDescritivo
     * @return 
     */
    public Decisao createDecisao(boolean decisao, String textoDescritivo) {
        return new Decisao(decisao, textoDescritivo);
    }
}
