package UI;

import Models.Atribuicao;
import Models.Candidatura;
import Models.Evento;
import Models.FAE;
import java.util.List;

public interface AlgoritmoAtribuicaoUI {

    public List<Atribuicao> criarAtribuicoes(List<Candidatura> listaCandidaturas, List<FAE> listaFAE);

    /**
     * Método que permite modificar a descrição do mecanismo de distribuição, recebendo a nova como parâmetro.
     * @param descricao nova descrição mecanismo
     */
    public void setDescricao(String descricao);

    /**
     * Método que devolve a descrição.
     * @return 
     */
    public String getDescricao();

}