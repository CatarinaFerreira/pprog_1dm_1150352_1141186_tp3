
package UI;

import Models.Data;

public interface SubmeterCandidaturaUI {
    
    public void getListaEventosSubmissao(Data data);
    
    public void selectEvento(int index);
    
    public boolean validaEvento();
    
    public void getUtilizador(String email);
    
    public void setDados(String nome, String morada, int telemovel);
    
    public void createCandidatura();
    
    public boolean validaCandidatura();
    
    public void addCandidatura();
}
