
package UI;

import Models.Candidatura;
import java.util.List;

public interface DecidirCandidaturaUI {
    
    public void getUtilizador(String email);

    public void getListaEventosFAE();

    public void selectEvento(int index);

    public List<Candidatura> getListaCandidaturasFAE();

    public void selectCandidatura(int index);

    public void createDecisao(boolean decisao, String textoDescritivo);

    public boolean validaDecisao();

    public void addDecisao();
}
