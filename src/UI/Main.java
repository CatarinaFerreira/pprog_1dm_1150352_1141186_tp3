/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import GUI.JanelaMenu;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Evento;
import Models.FAE;
import Models.FicheiroCentroEventos;
import Models.Organizador;
import Models.Utilizador;
import Models.Data;

public class Main {

    public static void main(String[] args) {
        CentroEventos centroEventos = new CentroEventos();

        Utilizador util = new Utilizador("joao", "joao@gmail.com", "123", "joao1");
        Utilizador util2 = new Utilizador("ze", "ze@gmail.com", "123", "ze1");
        Utilizador util3 = new Utilizador("miguel", "ze@gmail.com", "123", "miguel1");
        Utilizador util4 = new Utilizador("bino", "ze@gmail.com", "123", "bino1");
        Utilizador util5 = new Utilizador("beto", "ze@gmail.com", "123", "beto1");
        centroEventos.getListaUtilizadores().add(util);
        centroEventos.getListaUtilizadores().add(util2);
        centroEventos.getListaUtilizadores().add(util3);
        centroEventos.getListaUtilizadores().add(util4);
        centroEventos.getListaUtilizadores().add(util5);
        
        Data data1 = new Data(2017, 05, 02);
        Data data2 = new Data(2017, 05, 01);
        Data data3 = new Data(2017, 05, 15);
        
        Evento evento1 = new Evento("Evento1", "Texto Descritivo", "ISEP", data2, data3, data1);
        Evento evento2 = new Evento("Evento1", "Texto Descritivo", "ISEP", data2, data3, data1);

        FAE fae = new FAE(util);
        FAE fae1 = new FAE(util2);
        FAE fae2 = new FAE(util3);
        FAE fae3 = new FAE(util4);
        FAE fae4 = new FAE(util5);
//        evento1.getListaFAE().add(fae);
        evento1.getListaFAE().add(fae1);
        evento1.getListaFAE().add(fae2);
        evento1.getListaFAE().add(fae3);
        evento1.getListaFAE().add(fae4);
        
        evento2.getListaFAE().add(fae1);
        evento2.getListaFAE().add(fae2);
        evento2.getListaFAE().add(fae3);
        
        Candidatura c1 = new Candidatura(util2, "morada1", "Candidatura_1", 918969185);
        Candidatura c2 = new Candidatura(util3, "morada2", "Candidatura_2", 962548053);
        Candidatura c3 = new Candidatura(util4, "morada3", "Candidatura_3", 964775265);
        Candidatura c4 = new Candidatura(util5, "morada4", "Candidatura_4", 966566768);
        evento1.getListaCandidaturas().add(c1);
        evento1.getListaCandidaturas().add(c2);
        evento1.getListaCandidaturas().add(c3);
        evento1.getListaCandidaturas().add(c4);
        
        evento1.getListaOrganizadores().add(new Organizador(util));
        evento2.getListaOrganizadores().add(new Organizador(util));
        
        centroEventos.getListaEventos().add(evento1);

        FicheiroCentroEventos ficheiroCentroEventos = new FicheiroCentroEventos();

        JanelaMenu janelaMenu = new JanelaMenu(centroEventos, ficheiroCentroEventos);
         
    }
}
