///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Utils;
//
//import Lists.ListaCandidaturas;
//import Lists.ListaEventos;
//import Lists.ListaFAE;
//import Lists.ListaOrganizadores;
//import Lists.ListaUtilizadores;
//import Models.Candidatura;
//import Models.CentroEventos;
//import Models.FAE;
//import Models.Organizador;
//import Models.Utilizador;
//import UI.RegistarUtilizadorUI;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Scanner;
//
//public class Descodificador_2 {
//
//    private String fileListaEvento;
//    private String fileListaUtilizadores;
//    //private String fileListaWorkshops;
//    private String fileListaFAE;
//    private String fileListaOrganizador;
//    private String fileListaCandidatura;
//    private String fileListaRepresentate;
//
//    private static final String FILE_EVENTO_DEFAULT = "configuracoesGuardadas/listEvento.txt";
//    private static final String FILE_UTILIZADORES_DEFAULT = "configuracoesGuardadas/listUtilizadores.txt";
//    private static final String FILE_FAE_DEFAULT = "configuracoesGuardadas/listFAE.txt";
//    private static final String FILE_ORGANIZADORES_DEFAULT = "configuracoesGuardadas/listaOrganizadores.txt";
//    private static final String FILE_CANDIDATURA_DEFAULT = "configuracoesGuardadas/listCandidatura.txt";
//    private static final String FILE_REPRESENTANTE_DEFAULT = "configuracoesGuardadas/listaRepresentante.txt";
//    private static final String SEPARADOR = "#";    //usado para dividir diverentes informaçãos nos ficheiros
//    private static final String SEPARADOR_LISTA = "§";    //dentro das divisoes normais pode haver ","
//
//    /**
//     * Com a localização default dos ficheiros conf
//     */
//    public Descodificador_2() {
//        this.fileListaEvento = FILE_EVENTO_DEFAULT;
//        this.fileListaUtilizadores = FILE_UTILIZADORES_DEFAULT;
//        this.fileListaFAE = FILE_FAE_DEFAULT;
//        this.fileListaOrganizador = FILE_ORGANIZADORES_DEFAULT;
//        this.fileListaCandidatura = FILE_CANDIDATURA_DEFAULT;
//        this.fileListaRepresentate = FILE_REPRESENTANTE_DEFAULT;
//    }
//
//    /**
//     * Caso seja necessario guardar mais que um profile/base de
//     * centroEventos
//     *
//     * @param fileListaEvento
//     * @param fileListaUtilizadores
//     * @param fileListaFAE
//     * @param fileListaOrganizador
//     * @param fileListaCandidatura
//     * @param fileListaRepresentate
//     */
//    public  Descodificador_2(String fileListaEvento,
//            String fileListaUtilizadores, 
//            String fileListaFAE, 
//            String fileListaOrganizador, 
//            String fileListaCandidatura,
//            String fileListaRepresentate) {
//        this.fileListaEvento = fileListaEvento;
//        this.fileListaUtilizadores = fileListaUtilizadores;
//        this.fileListaFAE = fileListaFAE;
//        this.fileListaOrganizador = fileListaOrganizador;
//        this.fileListaCandidatura = fileListaCandidatura;
//        this.fileListaRepresentate = fileListaRepresentate;
//
//    }
//
//    /**
//     *
//     * @return
//     */
//    public String getFileListaEvento() {
//        return fileListaEvento;
//    }
//
//    /**
//     *
//     * @param fileListaEvento
//     */
//    public void setFileListaEvento(String fileListaEvento) {
//        this.fileListaEvento = fileListaEvento;
//    }
//    
//    /**
//     *
//     * @return
//     */
//    public String getFileListaUtilizadores() {
//        return fileListaUtilizadores;
//    }
//
//    /**
//     *
//     * @param fileListaUtilizadores
//     */
//    public void setFileListaUtilizadores(String fileListaUtilizadores) {
//        this.fileListaUtilizadores = fileListaUtilizadores;
//    }
//
//
//    /**
//     *
//     * @return
//     */
//    public String getFileListaFAE() {
//        return fileListaFAE;
//    }
//
//    /**
//     *
//     * @param fileListaFAE
//     */
//    public void setFileListaFAE(String fileListaFAE) {
//        this.fileListaFAE = fileListaFAE;
//    }
//
//    /**
//     *
//     * @return
//     */
//    public String getFileListaOrganizador() {
//        return fileListaOrganizador;
//    }
//
//    /**
//     *
//     * @param fileListaOrganizador
//     */
//    public void setFileListaOrganizador(String fileListaOrganizador) {
//        this.fileListaOrganizador = fileListaOrganizador;
//    }
//
//    /**
//     *
//     * @return
//     */
//    public String getFileListaCandidatura() {
//        return fileListaCandidatura;
//    }
//
//    /**
//     *
//     * @param fileListaCandidatura
//     */
//    public void setFileListaCandidatura(String fileListaCandidatura) {
//        this.fileListaCandidatura = fileListaCandidatura;
//    }
//
//    /**
//     *
//     * @return
//     */
//    public String getFileListaRepresentate() {
//        return fileListaRepresentate;
//    }
//
//    /**
//     *
//     * @param fileListaRepresentate
//     */
//    public void setFileListaRepresentate(String fileListaRepresentate) {
//        this.fileListaRepresentate = fileListaRepresentate;
//    }
//
//    /**
//     * Responsavel por carregar o sistema todo, recebe do main/gui/UI os objetos
//     * e acrescenta dados neles
//     *
//     * @param ce
//     * @param utilizadores
//     * @param organizadores
//     * @param fae
//     * @param representante
//     * @param candidatura
//     * @param eventos
//     */
//    public void run(CentroEventos ce, ListaUtilizadores utilizadores,
//            ListaOrganizadores organizadores, ListaFAE fae,Representante representante,
//            ListaCandidaturas candidatura, ListaEventos eventos) {
//        //SEM dependencias
//        utilizadores = carregaListaUtilizadores();
//
//        //Com dependecias
//        fae = carregaListaFAE(utilizadores);
//
//        candidatura = carregaListaCandidaturas(fae);
//        representante = carregaRepresentante(utilizadores);
//
//        organizadores = carregaListaOrganizadores(utilizadores);
//
//        eventos = carregaListaEventos(organizadores, fae, candidatura);
//
//        ce = carregaCentroEvento(eventos, utilizadores);
//
//    }
//
//    /**
//     *
//     * @param file Mesmo que consiga aceder ao ficheiro, msm assim o parametro
//     * serve para especificar qual do ficheiro necessario a ler
//     * @return
//     */
//    public List<String> leFile(String file) {
//        String aux;
//        List<String> linhas;
//        try (Scanner lerFicheiro = new Scanner(new File(file))) {
//            linhas = new ArrayList<>();   //não se sabe o tamanho do ficheiro, 
//            while (lerFicheiro.hasNext()) {
//                aux = ""; //limpa
//                aux = lerFicheiro.nextLine();  //recebe de ficheiro
//                if (!aux.isEmpty()) {
//                    linhas.add(aux);
//                }
//            }
//            lerFicheiro.close();
//        } catch (FileNotFoundException e) {
//            System.out.println("Ficheiro não existente.");
//            return null;
//        }
//
//        return linhas;
//    }
//
//    /**
//     * ;nome;email;username;
//     * password;confirmado;pontosExperiencia;nCargosExercidos)
//     *
//     * @return
//     */
//    public RegistaUtilizador carregaListaUtilizadores() {
//        int nCamposFicheiro = 7;
//        List<Utilizador> listaUtilizadores = new ArrayList<>();
//        List<String> linhas = leFile(fileListaUtilizadores);
//        if (linhas != null) {
//            for (String linhaTmp : linhas) {
//                if (!linhaTmp.isEmpty()) {
//                    String linha = linhaTmp.trim();    //uma limpeza pelo sim pelo não, tira um espaço a mais
//                    if (linha.contains(SEPARADOR)) {
//                        String[] dividido = linha.split(SEPARADOR);
//                        if (dividido.length == nCamposFicheiro) {
//                            try {    //o parse int poderia dar problemas se o id fosse mal escrito
//                                listaUtilizadores.add(new Utilizador(dividido[0],
//                                        dividido[1], dividido[2], dividido[3],
//                                        Boolean.parseBoolean(dividido[4]),
//                                        Integer.parseInt(dividido[5]),
//                                        Integer.parseInt(dividido[6])));
//                            } catch (NumberFormatException e) {     // zonas onde haveria numero é ignorado
//                                listaUtilizadores.add(new Utilizador(dividido[0],
//                                        dividido[1], dividido[2],
//                                        dividido[3]));
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return new RegistaUtilizador(listaUtilizadores);
//    }
//
//    /**
//     * Cria a lista de organizadores, adicionando utilizadores è semelhante ao
//     * metodo carregaListaFae, mas de tipo diferente
//     *
//     * @param user
//     * @return
//     */
//    public ListaOrganizadores carregaListaOrganizadores(RegistaUtilizador user) {
//        List<Organizador> listaOrganizador = new ArrayList<>();
//        List<String> linhas = leFile(fileListaOrganizador);   //cada linha so devera ter uma descrição
//        List<Utilizador> userTmp = user.getListaUtilizadores();
//        if (linhas != null) {
//            for (String linhasTmp : linhas) {
//                if (!linhasTmp.isEmpty()) {
//                    Utilizador u = retornaUserSeExiste(userTmp, linhasTmp);
//                    if (u != null) {   //se não vier vazio
//                        listaOrganizador.add(new Organizador(u));
//                    } //se não existe esse utilizador, o organiazador não é criado e adicionado a lista
//                }
//            }
//        }
//        return new ListaOrganizadores(listaOrganizador);
//    }
//
//    /**
//     * Cria a lista de faes, adicionando utilizadores, so tem os emails de cada
//     * divididos por enters
//     *
//     * @param user
//     * @return
//     */
//    public ListaFAE carregaListaFAE(RegistarUtilizadorUI util) {
//        List<FAE> listaFae = new ArrayList<>();
//        List<String> linhas = leFile(fileListaFAE);   //cada linha so devera ter uma descrição
//        if (linhas != null) {
//            for (String linhasTmp : linhas) {
//                if (!linhasTmp.isEmpty()) {
//                    Utilizador u = retornaUserSeExiste(util.getListaUtilizadores(), linhasTmp);
//                    if (u != null) {   //se não vier vazio
//                        listaFae.add(new FAE(u));
//                    } //se não existe esse utilizador, o fae não é criadoe  adicionado a lista
//                }
//            }
//        }
//
//        return new ListaFAE(listaFae);
//    }
////    
////      public ListaWorkshop carregaListaWorkshops() {
////        List<Workshop> listaWorkshops = new ArrayList<>();
////        List<String> linhas = leFile(fileListaOrganizador);   //cada linha so devera ter uma descrição
////        if (linhas != null) {
////            for (String linhasTmp : linhas) {
////                if (!linhasTmp.isEmpty()) {
////                        listaWorkshops.add(new Workshop(""));
////                    }
////                }
////            }
////        
////        return new ListaWorkshop(listaWorkshops);
////    }
//
//    /**
//     *
//     * @param userTmp
//     * @param email
//     * @return
//     */
//    public Utilizador retornaUserSeExiste(List<Utilizador> userTmp, String email) {
//        for (Utilizador utilizador : userTmp) {
//            if (utilizador.getEmail().equalsIgnoreCase(email)) {
//                return utilizador;
//            }
//        }
//        return null;  //fae não existe
//    }
//
//    /**
//     * So deverá existir um
//     *
//     * @param user
//     * @return
//     */
//    public Representante carregaRepresentante(RegistarUtilizadorUI user) {
//        List<String> linhas = leFile(fileListaRepresentate);   //cada linha so devera ter uma descrição
//
//        if (linhas != null) {
//            for (String linhasTmp : linhas) {
//                if (!linhasTmp.isEmpty()) {
//                    Utilizador u = retornaUserSeExiste(user.getListaUtilizadores(), linhasTmp);
//                    if (u != null) {   //se não vier vazio
//                        return new Representante(u);
//                    }
//                }
//            }
//        }
//        return null;
//    }
//
//    /**
//     * DADOS:
//     * ID;nomeComercialEmpresa;morada;telemovel;
//     * decisao;
//     * textoJustificativo;fae;Decisao;Justificacao
//     *
//     * @param fae
//     * @return
//     */
//    public ListaCandidaturas carregaListaCandidaturas(ListaFAE fae) {
//        int nCamposFicheiro = 7;   //avaliação é smp carregada vazia, faz-se new
//        int maxId = 1111;
//        List<Candidatura> listaCandidatura = new ArrayList<>();
//        List<String> linhas = leFile(fileListaCandidatura);
//        for (String linhaTmp : linhas) {
//            if (linhaTmp.contains(SEPARADOR)) { //LOGICAMENTE se o contain vem true é porq não esta vazio
//                String[] dividido = linhaTmp.trim().split(SEPARADOR);
//                if (dividido.length == nCamposFicheiro) {
//                    try {
//                        ListaFAE faeTmp = retornaListaFaeSeExiste(fae.getFAE(), dividido[6]);    //vê se o  nome do fae existe, se sim adicionao
//                        if (faeTmp != null) { // existe
//                            try {
//                                maxId = maxId(maxId, dividido[0]);
//                                /*
//                                 *  int id                              DIVIDIDO[0]
//                                 *  STRING nome                         DIVIDIDO[1]
//                                 *  STRING morada                       DIVIDIDO[2]
//                                 *  telemovel                           DIVIDIDO[3]
//                                 *  decisao                             DIVIDIDO[4]
//                                 *  textoJustificativo                  DIVIDIDO[5]
//                                 *  Lista fae                           DIVIDIDO[6]
//                                 */
//                                 listaCandidatura.add(new Candidatura(Integer.parseInt(dividido[0]),
//                                         dividido[1], 
//                                         dividido[2],
//                                         dividido[3], 
//                                          dividido[4], 
//                                         dividido[5],  
//                                         faeTmp, maxId));
//                               
//                            } catch (NumberFormatException e) {
//                                System.out.println("-->" + e);
//                            }
//                        }
//
//                    } catch (NumberFormatException e) {     // zonas onde haveria numero é ignorado
//                        System.out.println("->" + e);
//                    }
//                }
//            }
//        }
//        return new ListaCandidaturaS(listaCandidatura);
//    }
//
//    /**
//     * Garante que o id, default no contador
//     *
//     * @param maxID
//     * @param id
//     * @return
//     */
//    public int maxId(int maxID, String id) {
//        if (maxID < Integer.parseInt(id)) {
//            maxID = Integer.parseInt(id);
//        }
//        maxID++;
//        return maxID;
//    }
//
//    /**
//     *
//     * @param fae
//     * @param texto
//     * @return
//     */
//    public ListaFAE retornaListaFaeSeExiste(List<FAE> fae, String texto) {
//        String[] split = texto.split(SEPARADOR_LISTA);
//        List<FAE> listaFaeFinal = new ArrayList<>();
//        ListaFAE listaFae = new ListaFAE(listaFaeFinal);
//        for (String split1 : split) {
//            for (FAE faeTmp : fae) {
//                if (faeTmp.getUtilizador().getEmail().equalsIgnoreCase(split1)) {
//                    //se o nome for igual é porq essse fae existe
//                    listaFae.addFAE(faeTmp.getUtilizador());
//                }
//            }
//        }
//        return listaFae;
//    }
//
//    /**
//     * DADOS:
//     *
//     *
//     * @param listaOrganizador
//     * @param listaFae
//     * @param listaCandidatura
//     * @return
//     */
//    public RegistaEvento carregaListaEventos(ListaOrganizador listaOrganizador,
//            ListaFAE listaFae,
//            ListaCandidatura listaCandidatura) {
//        List<Evento> listaEventos = new ArrayList<>();
//        List<String> linhas = leFile(fileListaEvento);
//        if (linhas != null) {
//            for (String linhaTmp : linhas) {
//                if (linhaTmp.contains(SEPARADOR)) { //LOGICAMENTE se o contain vem true é porq não esta vazio
//                    String[] dividido = linhaTmp.trim().split(SEPARADOR);
//                    /*      LOGICA ENTRADA:    DIVIDIDO[x], sendo X:
//                titulo;                                 POSICAO 0
//                descricao;                              POSICAO 1
//                dataInicio;                             POSICAO 2                DATE.valueOf(String s)
//                dataFim;                                POSICAO 3
//                Local;                                  POSICAO 4
//                ListaOrganizador listaOrganizadores;    POSICAO 5
//                ListaFAE listaFAE;                      POSICAO 6
//                ListaCandidatura listaCandidatura;      POSICAO 7
//                     */
//                    try {
//                        listaEventos.add(new Evento(dividido[0],
//                                dividido[1],
//                                new Data(dividido[2]),// ex:   1996-02-03, ele converte para o formato esoftUtils.DATA
//                                new Data(dividido[3]),
//                                dividido[4],
//                                retornaListaDeOrganizadoresSeExistir(dividido[5], listaOrganizador),
//                                retornaListaDeFAESeExistir(dividido[6], listaFae),
//                                retornaListaDeCandidaturaSeExistir(dividido[7], listaCandidatura)));
//                    } catch (NumberFormatException e) {
//
//                    }
//
//                }
//            }
//        }
//
//        return new RegistaEvento(listaEventos);
//    }
//    
//    
//
//    /**
//     * Retorna uma lista de Organizadores segundo o ficheiro, MAS os
//     * organizadores têm q existir previamente
//     *
//     * @param analisar (Organizador1,Organizador2,Organizador3,Organizador4)
//     * @param listaOrganizador
//     * @return
//     */
//    public ListaOrganizadores retornaListaDeOrganizadoresSeExistir(String analisar,
//            ListaOrganizadores listaOrganizador) {
//        String[] tmp = analisar.split(SEPARADOR_LISTA);
//        List<Organizador> listaOrganizadorRetornar = new ArrayList<>();
//        for (String tmp1 : tmp) {//pesquisa segundo cada Organizador a acrescentar
//            
//            for (Organizador organizadorTmp : listaOrganizador.getListaOrganizadores()) {
//                if (organizadorTmp.getUtilizador().getEmail().equalsIgnoreCase(tmp1) && !(tmp1.isEmpty())) {
//                    listaOrganizadorRetornar.add(organizadorTmp);         //se existe um organizador com esse email, este é acrescentado á nova lista
//                }
//            }
//        }
//        return new ListaOrganizadores(listaOrganizadorRetornar);
//    }
//
//    /**
//     *
//     * @param analisar
//     * @param listaFae
//     * @return
//     */
//    public ListaFAE retornaListaDeFAESeExistir(String analisar, ListaFAE listaFae) {
//        String[] tmp = analisar.split(SEPARADOR_LISTA);
//        List<FAE> listaFaeRetornar = new ArrayList<>();
//        for (String tmp1 : tmp) {//pesquisa segundo cada fae a acrescentar
//            
//            for (FAE faeTmp : listaFae.getFAE()) {
//                if (faeTmp.getUtilizador().getEmail().equalsIgnoreCase(tmp1) && !(tmp1.isEmpty())) {
//                    listaFaeRetornar.add(faeTmp);   //se esse fae existe é acrescentado a lista
//                }
//            }
//        }
//        return new ListaFAE(listaFaeRetornar);
//    }
//
//    /**
//     *
//     * @param analisar
//     * @param listaWorkshops
//     * @return
//     */
////    public ListaWorkshop retornaListaDeWorkshopsSeExistir(String analisar,
////            ListaWorkshop listaWorkshops) {
////        String[] tmp = analisar.split(SEPARADOR_LISTA);
////        List<Workshop> listaWorkshopsRetornar = new ArrayList<>();
////        for (String tmp1 : tmp) {
////            
////            for (Workshop WorkshopTmp : listaWorkshops.getListaWorkshop()) {
////                if (WorkshopTmp.getId() == Integer.parseInt(tmp1) && !(tmp1.isEmpty())) {
////                    listaWorkshopsRetornar.add(WorkshopTmp);   //se esse workshop existe é acrescentado a lista
////                }
////            }
////        }
////        return new ListaWorkshop(listaWorkshopsRetornar);
////    }
//
//
//    /**
//     *
//     * @param analisar
//     * @param listaCandidatura
//     * @return
//     */
//    public ListaCandidaturas retornaListaDeCandidaturaSeExistir(String analisar, 
//            ListaCandidaturas listaCandidaturas) {
//        String[] tmp = analisar.split(SEPARADOR_LISTA);
//        List<Candidatura> listaCandidaturaRetornar = new ArrayList<>();
//        for (String tmp1 : tmp) {//pesquisa segundo cada fae a acrescentar
//            
//            for (Candidatura candidaturaTmp : listaCandidaturas.getListaCandidaturas()) {
//                if (candidaturaTmp.getId() == Integer.parseInt(tmp1) && !(tmp1.isEmpty())) {
//                    listaCandidaturaRetornar.add(candidaturaTmp); 
//                }
//            }
//        }
//        return new ListaCandidaturas(listaCandidaturaRetornar);
//    }
//
//    /**
//     * Instancia um CentroEventos ja criado previamento, adicionando os
//     * diferentes objetos q ele necessita para funcionar
//     *
//     //* @param rRecurso
//     * @param registroEvento
//     * @param rUtilizador
//     * @return
//     */
//    public CentroEventos carregaCentroEvento(RegistaEvento registroEvento, RegistaUtilizadorUI rUtilizador) {
//
//        return new CentroEventos(registroEvento, rUtilizador);
//    }
//
//}

