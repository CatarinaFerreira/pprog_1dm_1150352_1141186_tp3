/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import GUI.DialogoEscolhaFicheiro;
import Models.AlgoritmoAtribuicaoEquitativa;
import Models.AlgoritmoAtribuicaoExperiencia;
import Models.AlgoritmoAtribuicaoNumeroFAE;
import Models.CentroEventos;
import Models.Utilizador;
import java.io.File;
import javax.swing.JOptionPane;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 * This contains important methods for write and read from a file
 */
public class Descodificador {

    public static String NAME_OF_DEFAULT_FILE_DATA = "data-cde.bin";

    /**
     * Write an object that was passed from the parameter to a binary file
     * (*.bin)
     *
     * @param fileName File name
     * @param cde Centro de Exposicoes
     */
    public static void writeObject(String fileName, CentroEventos cde) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            try {
                out.writeObject(cde);
            } finally {
                out.close();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Não foi possível a escrita do ficheiro\n" + ex.getMessage(),
                    "Erro",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Read from a binary file to load the object to the memory. If the binary
     * file (*.bin) isnt ready to load, the sistem will ask if the user wants to
     * read from a .txt file pre-writed. If yes, the sistem will generate a
     * binary file to load the object to the memory
     *
     * @param fileName File name
     * @return Object that is read
     */
    public static CentroEventos readObject(String fileName) {
        CentroEventos cde = null;
        boolean exception;
        do {
            exception = false;
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
                try {
                    cde = (CentroEventos) in.readObject();
                } finally {
                    in.close();
                }
            } catch (ClassNotFoundException | IOException ex) {
                exception = true;
                int option = JOptionPane.showConfirmDialog(null,
                        "Ficheiro de persistência de dados não encontrado."
                        + "\nDeseja inicializar através do ficheiro de configuração?",
                        "Erro de Leitura",
                        JOptionPane.YES_NO_OPTION);
                if (option == 1) { //NO_OPTION_DIALOG_FRAME
                    System.exit(1);
                }
                DialogoEscolhaFicheiro fc = new DialogoEscolhaFicheiro();
                boolean exceptionExtention;
                try {
                    exceptionExtention = false;
                    if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        fc.saveFileInitializer();
                    } else {   
                        System.exit(0);
                    }
                } catch (IllegalArgumentException exExtention) {
                    exceptionExtention = true;
                    JOptionPane.showMessageDialog(null, exExtention.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        } while (exception);
        return cde;
    }

    /**
     * This mechanism imports from a pre-configure text file (*.txt) to
     * initialize the objects of the Centro de Exposicoes.
     * @param file A reference to a text file
     * @return an object that represents Centro de Eventos
     */
    public static CentroEventos importFileInitializer(File file) {
        CentroEventos cde = new CentroEventos();
        cde.addAlgoritmo(new AlgoritmoAtribuicaoNumeroFAE());
        cde.addAlgoritmo(new AlgoritmoAtribuicaoEquitativa());
        cde.addAlgoritmo(new AlgoritmoAtribuicaoExperiencia());
        try {
            Scanner r = new Scanner(file, "UTF-8");
            try {
                String line;
                while (r.hasNext()) {
                    line = getNextLine(r);
                    if (line.contains("-Utilizador")) {
                        userInitialization(line, cde);
//                    } else if (line.contains("-Evento")) {
//                        eventsInitialization(r, line, cde);
                    }
                }
            } finally {
                r.close();
            }
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Ficheiro de inicialização não existe", "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(null, "Ficheiro Corrompido (" + ex.getMessage() + ")", "ERRO", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        return cde;
    }

    /**
     * Auxiliar method that initialize from a text file (.txt) an Utilizador
     *
     * @param line line that was readed
     * @param cde a Centro de Exposicoes
     */
    private static void userInitialization(String line, CentroEventos cde) {
        line = line.replace("\t-Utilizador{", "").replace("}", "");
        String[] attributes = line.split(", ");
        if (attributes.length == 4) {
            Utilizador u = new Utilizador(attributes[0].split("=")[1], attributes[1].split("=")[1], attributes[2].split("=")[1], attributes[3].split("=")[1]);
            cde.getListaUtilizadores().add(u);
        }
    }

//    /**
//     * Auxiliar method that initialize from a text file (.txt) an Utilizador
//     *
//     * @param r the input streamer
//     * @param line the line that was readed
//     * @param cde the Centro de Exposicoes
//     */
//    private static void eventsInitialization(Scanner r, String line, CentroEventos cde) {
//        Evento e = null;
//        line = line.replace("-Evento{", "").replace("}", "").trim();
//        String[] attributes = line.split(", ");
//        if (attributes.length == 4) {
////            e = new Evento(attributes[0].split("=")[1], attributes[1].split("=")[1], attributes[2].split("=")[1], attributes[3].split("=")[1]);
//            line = getNextLine(r);
//            while (line.contains("- Candidatura") || line.contains("- Organizador") || line.contains("- FAE")) {
//                if (line.contains("- Candidatura")) {
//                    line = line.replace("- Candidatura{", "").replace("}", "").trim();
//                    attributes = line.split(", ");
//                    if (attributes.length == 5) {
//                        Candidatura c = new Candidatura((attributes[0].split("=")[1]), attributes[1].split("=")[1], attributes[2].split("=")[1], Integer.parseInt(attributes[3].split("=")[1]));
//                        e.getListaCandidaturas().add(c);
//                    }
//                }
//                if (line.contains("-Organizador")) {
//                    line = line.replace("- Organizador{", "").replace("}", "").trim();
//                    attributes = line.split(", ");
//                    if (attributes.length == 1) {
//                        Organizador o = new Organizador(cde.getListaUtilizadores().getUtilizador(attributes[0].split("=")[1]));
//                        e.getListaOrganizadores().add(o);
//                    }
//                }
//                if (line.contains("-FAE")) {
//                    line = line.replace("- FAE{", "").replace("}", "").trim();
//                    attributes = line.split(", ");
//                    if (attributes.length == 1) {
//                        FAE f = new FAE((Utilizador) cde.getListaUtilizadores().getUtilizador(attributes[0].split("=")[1]));
//                        e.getListaFAE().add(f);
//                    }
//                }
//                line = getNextLine(r);
//            }
//            cde.getListaEventos().add(e);
//        }
//    }

    /**
     * A personalize next line, that ignor all the line that have "#", because
     * this is a comment
     *
     * @param r The input streamer
     * @return Line was readed
     */
    private static String getNextLine(Scanner r) {
        String line = "";
        while (r.hasNext()) {
            line = r.nextLine().trim();
            if (!line.contains("#")) {
                if (line.isEmpty()) {
                    line = "";
                }
                return line;
            }
        }
        return line;
    }
}