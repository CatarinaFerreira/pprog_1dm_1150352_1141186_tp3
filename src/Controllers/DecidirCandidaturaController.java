package Controllers;

import Exceptions.IllegalAccessCandidaturasPorAtribuir;
import GUI.IniciarSessaoGUI;
import Lists.ListaAtribuicoes;
import Lists.ListaCandidaturas;
import Models.Atribuicao;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Decisao;
import Models.Evento;
import Models.FAE;
import Models.Utilizador;
import UI.DecidirCandidaturaUI;
import java.util.ArrayList;
import java.util.List;

public class DecidirCandidaturaController implements DecidirCandidaturaUI {

    private CentroEventos centroEventos;
    private List<Evento> listaEventos;
    private Evento evento;                                                                                                                                                  
    private List<Candidatura> listaCandidaturas;
    private ListaAtribuicoes listaA;
    private ListaCandidaturas listaC;
    private Candidatura candidatura;
    private Decisao decisao;
    private Utilizador utilizador;
    private FAE fae;
    
    public DecidirCandidaturaController(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }
    
    @Override
    public void getUtilizador(String email) {
        utilizador = centroEventos.getUtilizadorEmail(email);
    }
    
    @Override
    public void getListaEventosFAE() {
        listaEventos = centroEventos.getListaEventosFAE(utilizador);
    }

    @Override
    public void selectEvento(int index) {
        evento = listaEventos.get(index);
    }
//    
//    public void setDecisao(String dec) {
//        decisao.setDecisao(true);
//    }
//
//    public void setJustificao(String justificao) {
//        decisao.setTextoDescritivo(justificao);
//    }
 
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
    
    @Override
    public List<Candidatura> getListaCandidaturasFAE() {
        return evento.getListaCandidaturasFAE(utilizador);
    }

    @Override
    public void selectCandidatura(int index) {
        candidatura = listaCandidaturas.get(index);
    }

    /**
     * Obter as candidaturas consoante o utilizador guardando no controller
     */
    public void obterCandidaturas() {
        this.listaA = evento.getListaAtribuicoes();
        this.listaCandidaturas = new ArrayList<>();
        for (Atribuicao a : listaA.getListaAtribuicoes()) {
            if (IniciarSessaoGUI.SelectedUser.getUsername().equals(a.getFAE().getUtilizador().getUsername())) {
                if (!a.getCandidatura().decididaPeloFae(a.getFAE())) {
                    listaCandidaturas.add(a.getCandidatura());
                }
            }
        }
        if (listaCandidaturas.isEmpty()) {
            throw new IllegalAccessCandidaturasPorAtribuir("De momento, não existem candidaturas por decidir");
        }
    }
    
    @Override
    public void createDecisao(boolean decisao_boolean, String textoDescritivo) {
        decisao = candidatura.createDecisao(decisao_boolean, textoDescritivo);
    }

    @Override
    public boolean validaDecisao() {
        return decisao.validaDecisao();
    }

    @Override
    public void addDecisao() {
        candidatura.addDecisao(decisao);
    }
    
    /**
     * Regista a decisão tomada.
     * @param c candidatura sobre a qual foi tomada uma decisão.
     */
    public void registarDecisao(Candidatura c) {
        listaC.registarDecisao(c, decisao);
        fae.retiraCarga();
        //Remover das candidaturas (locais), uma vez que já foram avaliadas pelo FAE
        listaCandidaturas.remove(c);
    }
  
    /**
     * Verifica se o evento está definido.
     *
     * @return boolean
     */
    public boolean hasEvento() {
        return evento != null;
    }
}
