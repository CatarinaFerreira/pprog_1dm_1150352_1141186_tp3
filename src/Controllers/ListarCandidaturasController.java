/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Candidatura;
import Models.Evento;
import java.util.List;

public class ListarCandidaturasController {
    
    private Evento e;
    
    public ListarCandidaturasController(Evento evento) {
        this.e=evento;
    }
    
    public List<Candidatura> getListaCandidaturas() {
        return e.getListaCandidaturas();
    }   
}
