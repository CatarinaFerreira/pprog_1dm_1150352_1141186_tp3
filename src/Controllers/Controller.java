/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Evento;
import java.util.List;

public interface Controller {
  
    /**
     * Lista de Eventos associados ao contexto.
     *
     * @return eventos
     */
    public List<Evento> listarEventos();

    /**
     * Definir Evento.
     *
     * @param e evento a definir.
     */
    public void definirEventos(Evento e);  
}
