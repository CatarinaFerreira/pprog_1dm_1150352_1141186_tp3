/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.util.ArrayList;
import java.util.List;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Decisao;
import Models.Evento;

public class ListarDecisoesController {
 
    private CentroEventos ce;
    
    public ListarDecisoesController(CentroEventos ce) {
        this.ce=ce;
    }
    
    public List<Decisao> getListaDecisoes() {
        List<Candidatura> list = (List<Candidatura>) ce.getListaCandidaturas();
        for (Evento e :ce.getListaEventos()) {
             list.addAll(e.getListaCandidaturas());
        }
        List<Decisao> result=new ArrayList();
        for (Candidatura element:list) {
            result.add((Decisao) element.getListDecisoes());
        }
        return result;
    }  
}
