
package Controllers;

import Lists.ListaCandidaturas;
import Models.Candidatura;
import Models.Evento;
import UI.SubmeterCandidaturaUI;
import java.util.List;
import Models.CentroEventos;
import Models.Data;
import Models.Utilizador;

public class SubmeterCandidaturaController implements SubmeterCandidaturaUI{

    private CentroEventos centroEventos;
    private List<Evento> listaEventosSubmissao;
    private Evento evento;
    private Candidatura candidatura;
    private Utilizador utilizador;
    private String morada;
    private String nome;
    private int telemovel;
    
    public SubmeterCandidaturaController(CentroEventos ce) {
        this.centroEventos=ce;
    }
    
    @Override
    public void getUtilizador(String email) {
        utilizador = centroEventos.getUtilizadorEmail(email);
    }
    
    @Override
    public void getListaEventosSubmissao(Data data) {
        listaEventosSubmissao = centroEventos.getListaEventosSubmissao(data);
    }

    @Override
    public void selectEvento(int index) {
        evento = listaEventosSubmissao.get(index);
    }

    @Override
    public boolean validaEvento() {
        return evento.validaEventoParaCandidatura(utilizador);
    }
    
    @Override
    public void setDados(String nome, String morada, int telemovel) {
        this.candidatura.setNome(nome);
        this.candidatura.setMorada(morada);
        this.candidatura.setTelemovel(telemovel);
    }
    
    @Override
    public void createCandidatura() {
        candidatura = evento.createCandidatura(utilizador, morada, nome, telemovel);
    }

    public void novaCandidatura(){
        this.candidatura = this.centroEventos.getListaCandidaturas().novaCandidatura();
    }
    
    @Override
    public boolean validaCandidatura() {
        return candidatura.validaDadosCandidatura() && evento.validaCandidatura(candidatura);
    }

    @Override
    public void addCandidatura() {
        evento.addCandidatura(candidatura);
    }
    
    public boolean registaCandidatura() {
        ListaCandidaturas lc = centroEventos.getListaCandidaturas();
        return (lc.validaCandidatura(candidatura)) ? lc.registarCandidatura(candidatura): false;
    } 
}
