package Controllers;

import Models.AlgoritmoAtribuicao;
import Models.Atribuicao;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Data;
import Models.Evento;
import Models.FAE;
import Models.Utilizador;
import UI.AlgoritmoAtribuicaoUI;
import UI.AtribuirCandidaturaUI;
import java.util.List;

public class AtribuirCandidaturaController implements AtribuirCandidaturaUI {

    private List<Evento> listaEventos;
    private Utilizador utilizador;
    private CentroEventos centroEventos;
    private Evento evento;
    private List<FAE> listaFAE;
    private List<Candidatura> listaCandidaturas;
    private List<Atribuicao> listaAtribuicoes;
    private List<AlgoritmoAtribuicao> listaAlgoritmos;
    private AlgoritmoAtribuicao algoritmo;

    /**
     *
     * @param centroEventos CentroEventos.
     */
    public AtribuirCandidaturaController(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }

    /**
     * Método que retorna a lista de atribuições.
     *
     * @param aa lista de atribuições.
     * @return candidatavel
     */
    public List<Atribuicao> getListaAtribuicoes(AlgoritmoAtribuicaoUI aa) {
        return aa.criarAtribuicoes(listaCandidaturas, listaFAE);
    }

    public List<Evento> getListaEventos(Utilizador u, Data data) {
        return centroEventos.getListaEventosOrganizadorAtribuicao(utilizador, data);
    }
    
    @Override
    public void getUtilizador(String email) {
        utilizador = centroEventos.getUtilizadorEmail(email);
    }

    @Override
    public List<Evento> getListaEventosOrganizadorAtribuicao(Utilizador utilizador, Data data) {
        this.listaEventos = centroEventos.getListaEventosOrganizadorAtribuicao(utilizador, data);
        return this.listaEventos;
    }

    @Override
    public void selectEvento(Evento evento) {
        this.evento = evento;
    }
    
    @Override
    public List<AlgoritmoAtribuicao> getListaAlgoritmos() {
        this.listaAlgoritmos = centroEventos.getListaAlgoritmos();
        return this.listaAlgoritmos;
    }
    
    @Override
    public List<Candidatura> getListaCandidaturas() {
        this.listaCandidaturas = evento.getListaCandidaturas();
        return this.listaCandidaturas;
    }
    
    @Override
    public List<FAE> getListaFAE() {
        this.listaFAE = evento.getListaFAE();
        return this.listaFAE;
    }

    @Override
    public void selectAlgoritmo(AlgoritmoAtribuicao algoritmo) {
        this.algoritmo = algoritmo;
    }
    
    @Override
    public List<Atribuicao> createListaAtribuicoes() {
        this.listaAtribuicoes = algoritmo.criarAtribuicoes(listaCandidaturas, listaFAE);
        return this.listaAtribuicoes;
    }
    
    @Override
    public boolean validaAtribuicoes() {
        return evento.validaAtribuicoes(listaAtribuicoes);
    }
    
    @Override
    public void addListaAtribuicoes() {
        evento.addListaAtribuicoes(listaAtribuicoes);
    }
}
