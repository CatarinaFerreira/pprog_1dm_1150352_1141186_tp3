/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.SubmeterCandidaturaController;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import Models.CentroEventos;
import Lists.ListaCandidaturas;
import java.awt.FlowLayout;
import javax.swing.JLabel;

public class DialogoSubmeterCandidaturaGUI extends JDialog {
    
    private JTextField txtNome;
    private JTextField txtMorada;
    private JTextField txtTelemovel;
    private final JFrame frame;
    private final JTabbedPane tabPane;
    
    private SubmeterCandidaturaController controller;
    private PainelListaCandidaturasGUI pListaCandidaturas;
    
    /**
     * Constrói a Janela que irá conter espaços indicados para o preenchimento dos dados necessários para a submissão
     * de uma candidatura.
     * @param frame frame.
     * @param listaCandidaturas lista candidaturas.
     * @param tabPane tab pane.
     * @param pListaCandidaturas painel lista lista candidaturas.
     * @param ce centro eventos.
     */
   
    public DialogoSubmeterCandidaturaGUI( JFrame frame, ListaCandidaturas listaCandidaturas, JTabbedPane tabPane, 
            PainelListaCandidaturasGUI pListaCandidaturas, CentroEventos ce) {

        super(frame, "Submeter Candidatura", true);

        controller = new SubmeterCandidaturaController(ce);
        final int NUMERO_LINHAS = 2, NUMERO_COLUNAS = 1;
        setLayout(new GridLayout(NUMERO_LINHAS,NUMERO_COLUNAS));
    
        this.frame = frame;
        this.tabPane = tabPane;
        this.pListaCandidaturas = pListaCandidaturas;
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        pack();
        setResizable(false);
        setLocationRelativeTo(frame);
        setVisible(true);
    }  
    
    /**
     * Cria e adiciona componentes à Janela.
     */
    
    private void criarComponentes(){
        JPanel p1 = criarPainelNome();
        JPanel p2 = criarPainelMorada();
        JPanel p3 = criarPainelTelemovel();
        JPanel p4 = criarPainelBotoes();   
        
        add(p1);
        add(p2);
        add(p3);
        add(p4);
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução do nome comercial do expositor.
     * @return 
     */
    
    private JPanel criarPainelNome() {
        JLabel lbl = new JLabel("Nome Comercial:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        txtNome = new JTextField(CAMPO_LARGURA);
        txtNome.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtNome);

        return p;
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução da morada do expositor.
     * @return 
     */
    
    private JPanel criarPainelMorada() {
        JLabel lbl = new JLabel ("Morada:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtMorada = new JTextField(CAMPO_LARGURA);
        txtMorada.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMorada);

        return p;
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução do telemóvel do expositor.
     * @return 
     */
    
    private JPanel criarPainelTelemovel() {
        JLabel lbl = new JLabel ("Telemóvel:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtTelemovel = new JTextField(CAMPO_LARGURA);
        txtTelemovel.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTelemovel);

        return p;    
    }
  
    /**
     * Constrói o painel que irá conter 2 botões que irão permitir guardar a candidatura ou cancelar a sua 
     * submissão.
     * @return 
     */   
    private JPanel criarPainelBotoes() {
        JButton btnGuardar = criarBotaoGuardar();
        getRootPane().setDefaultButton(btnGuardar);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 30;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnGuardar);
        p.add(btnCancelar);

        return p;
    }
    
    /**
     * Constrói o botão Guardar que irá registar os dados inseridos referentes à candidatura e adicionará os mesmos
     * ao separador correspondente.
     * @return 
     */
    
    private JButton criarBotaoGuardar() {
        JButton btn = new JButton("Guardar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    controller.novaCandidatura();
                    String nome = txtNome.getText();
                    String morada = txtMorada.getText();
                    int telemovel = Integer.parseInt(txtTelemovel.getText());
                    
                    controller.setDados(nome, morada, telemovel);
                    boolean candidaturaAdicionada = controller.registaCandidatura();
                    
                    if (candidaturaAdicionada) {
                        JOptionPane.showMessageDialog(
                                frame,
                                "Candidatura adicionada às candidaturas registadas.",
                                "Nova Candidatura",
                                JOptionPane.INFORMATION_MESSAGE);
                        pListaCandidaturas.onTabChanged();
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(
                                frame,
                                "Candidatura já incluída nas candidaturas registadas ou é invalida!",
                                "Nova Candidatura",
                                JOptionPane.ERROR_MESSAGE);
                    }

                    final int TAB_LISTA_CANDIDATURAS = 1;                    
                    tabPane.setSelectedIndex(TAB_LISTA_CANDIDATURAS);   
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(
                            frame,
                            "Tem que introduzir um número inteiro com 9 digitos.",
                            "Novo Telemovel",
                            JOptionPane.WARNING_MESSAGE);
                    txtTelemovel.requestFocusInWindow();
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(
                            frame,
                            ex.getMessage(),
                            "Nova Candidatura",
                            JOptionPane.WARNING_MESSAGE);
                    txtNome.requestFocusInWindow();
                }
            }
        });
        return btn;
    }
    
    /**
     * Constrói o botão Cancelar que cancela automaticamente a submissão da candidatura.
     * @return 
     */    
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
