/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.ListarDecisoesController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import Models.CentroEventos;
import Models.Decisao;
import UI.TabChangeListener;

public class PainelListaDecisoesGUI extends JPanel implements TabChangeListener {
    
    /**
     * Lista de decisões tomadas.
     */
    private JList<Decisao> list; 
    
    private DefaultListModel<Decisao> model;
    
    /**
     * Controller ListarDecisoes.
     */
    private ListarDecisoesController controller;
  
    /**
    * Constrói o painel que irá conter uma lista com todas as candidaturas.
    * 
    * @param ce centro eventos.
    */    
    public PainelListaDecisoesGUI(CentroEventos ce) {
        super();
        controller = new ListarDecisoesController(ce);
        setLayout(new BorderLayout());
        
        add(criarPainelListaCandidaturas(), BorderLayout.CENTER);
    }

    /**
     * Método que atualiza a lista de decisões tomadas.
     */   
    public void atualizarListaDecisoes(){
        model.clear();
        List<Decisao> decisoes = controller.getListaDecisoes();
        for (Decisao element:decisoes) {
            model.addElement(element);
        }
    }
    
    /**
     * Constrói o painél com a lista de candidaturas.
     * @return 
     */   
    private JScrollPane criarPainelListaCandidaturas() {
        model = new DefaultListModel();
        list = new JList();
        list.setModel(model);

        JScrollPane scrollPane = new JScrollPane(list);
        
        scrollPane.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createLineBorder(Color.GRAY,2,true), "Decisões Tomadas"));
        
        return scrollPane;
    }

    /**
     * Método que permite que, quando o utilizador selecione a opção Guardar, o visor alterne automaticamente para
     * o separador Decisoes.
     */  
    @Override
    public void onTabChanged() {
        atualizarListaDecisoes();
    }   
}
