/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.Controller;
import Controllers.DecidirCandidaturaController;
import Models.Candidatura;
import Models.CentroEventos;
import Exceptions.IllegalAccessCandidaturasPorAtribuir;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * JPanel that represents the use case Decidir Candidatura
 */
public class DECIDIRCANDIDATURA extends JPanel {

    //GLOBAL WINDOW COMPONENTS
    private JPanel centerPanel;
    private JPanel westPanel;
    private JPanel westListPanel;
    private JPanel westDecisionPanel;
    private JPanel eastPanel;
    private JPanel bottomPanel;
    private JPanel layerName;
    private JPanel layerAddress;
    private JPanel layerCellPhoneNumber;
    private JPanel layerArea;
    private JPanel layerInvitaitions;
    private JPanel layerProducts;
    private JPanel layerDemonstracao;
    private JPanel layerText;
    private JPanel layerRadioButtons;
    private JPanel layerLstCandidatures;
    private JLabel lblName;
    private JLabel lblAddress;
    private JLabel lblCellPhoneNumber;
    private JLabel lblArea;
    private JLabel lblInvitaitions;
    private JLabel lblProducts;
    private JLabel lblDemonstracao;
    private JLabel lblListCandidatures;
    private JTextField tfInputName;
    private JTextField tfInputAddress;
    private JTextField tfInputCellPhoneNumber;
    private JList<Candidatura> lstCandidatures;
    private ButtonGroup radioGroup;
    private JRadioButton rbAccept;
    private JRadioButton rbDecline;
    private JLabel lblText;
    private JTextArea taText;
    private JButton btnCancel;
    private JButton btnSubmit;

    //GLOBAL Variables
    /**
     * Controller of the use case
     */
    private DecidirCandidaturaController ctrl;
    //GLOBAL CONSTANTS
    /**
     * Main frame that contains this panel
     */
    private final Frame mainFrame;
    /**
     * Centro De Exposicoes
     */
    private final CentroEventos cde;

    /**
     * Constructor of the UI
     *
     * @param mainFrame
     * @param cde
     */
    public DECIDIRCANDIDATURA(Frame mainFrame, CentroEventos cde) {
        this.mainFrame = mainFrame;
        this.cde = cde;

        initializeComponents();
    }

    /**
     * Create, initialize and setup the components
     */
    private void initializeComponents() {
        //Containers of components

        westPanel = new JPanel();
        westPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        westPanel.setBackground(null);

        westListPanel = new JPanel();
        westListPanel.setLayout(new BoxLayout(westListPanel, BoxLayout.Y_AXIS));
        westListPanel.setBackground(null);
        westListPanel.setBorder(new TitledBorder("Candidaturas"));

        westDecisionPanel = new JPanel();
        westDecisionPanel.setLayout(new BoxLayout(westDecisionPanel, BoxLayout.Y_AXIS));
        westDecisionPanel.setBackground(null);
        westDecisionPanel.setBorder(new TitledBorder("Decisão"));

        eastPanel = new JPanel();
        eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.Y_AXIS));
        eastPanel.setBackground(null);
        eastPanel.setBorder(new TitledBorder("Candidatura"));

        centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());
        centerPanel.setBackground(null);

        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBackground(null);

        //Components and Layers
        final Dimension DIMENSION_TEXT_FIELDS = new Dimension(140, 30);
        final Dimension DIMENSION_LISTS = new Dimension(140, 100);
        final int HORIZONTAL_GAP = 20, VERTICAL_GAP = 20;
        final LayoutManager LAYOUT_LAYER_FORM = new FlowLayout(FlowLayout.TRAILING, HORIZONTAL_GAP, VERTICAL_GAP);
        //Colors of JTextFields and JLists
        final Color COLOR_BACKGROUND_DISABLE_COMPONENTS = Color.LIGHT_GRAY,
                COLOR_TEXT_DISABLE_COMPONENTS = Color.BLACK;

        layerLstCandidatures = new JPanel(LAYOUT_LAYER_FORM);
        lblListCandidatures = new JLabel("Lista de Candidaturas para decisão:");
        lstCandidatures = new JList<>();
        final Dimension DIMENSION_LIST_CANDIDATURES = new Dimension(200, 200);
        lstCandidatures.setPreferredSize(DIMENSION_LIST_CANDIDATURES);
        layerLstCandidatures.add(lblListCandidatures);
        layerLstCandidatures.add(lstCandidatures);
        layerLstCandidatures.setBackground(null);

        layerName = new JPanel(LAYOUT_LAYER_FORM);
        lblName = new JLabel("Nome:");
        tfInputName = new JTextField();
        tfInputName.setEditable(false);
        tfInputName.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputName.setBackground(COLOR_BACKGROUND_DISABLE_COMPONENTS);
        tfInputName.setForeground(COLOR_TEXT_DISABLE_COMPONENTS);
        layerName.add(lblName);
        layerName.add(tfInputName);
        layerName.setBackground(null);

        layerAddress = new JPanel(LAYOUT_LAYER_FORM);
        lblAddress = new JLabel("Morada:");
        tfInputAddress = new JTextField();
        tfInputAddress.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputAddress.setEditable(false);
        tfInputAddress.setBackground(COLOR_BACKGROUND_DISABLE_COMPONENTS);
        tfInputAddress.setForeground(COLOR_TEXT_DISABLE_COMPONENTS);
        layerAddress.add(lblAddress);
        layerAddress.add(tfInputAddress);
        layerAddress.setBackground(null);

        layerCellPhoneNumber = new JPanel(LAYOUT_LAYER_FORM);
        lblCellPhoneNumber = new JLabel("Telemovel:");
        tfInputCellPhoneNumber = new JTextField();
        tfInputCellPhoneNumber.setEditable(false);
        tfInputCellPhoneNumber.setPreferredSize(DIMENSION_TEXT_FIELDS);
        tfInputCellPhoneNumber.setBackground(COLOR_BACKGROUND_DISABLE_COMPONENTS);
        tfInputCellPhoneNumber.setForeground(COLOR_TEXT_DISABLE_COMPONENTS);
        layerCellPhoneNumber.add(lblCellPhoneNumber);
        layerCellPhoneNumber.add(tfInputCellPhoneNumber);
        layerCellPhoneNumber.setBackground(null);

        layerRadioButtons = new JPanel(LAYOUT_LAYER_FORM);
        rbAccept = new JRadioButton("Aceitar");
        rbDecline = new JRadioButton("Recusar");
        layerRadioButtons.add(rbAccept);
        layerRadioButtons.add(rbDecline);
        layerRadioButtons.setBackground(null);

        radioGroup = new ButtonGroup();
        radioGroup.add(rbAccept);
        radioGroup.add(rbDecline);
        radioGroup.setSelected(rbAccept.getModel(), true);
        radioGroup.setSelected(rbDecline.getModel(), false);

        layerText = new JPanel();
        lblText = new JLabel("Texto Justificativo:");
        taText = new JTextArea();
        taText.setPreferredSize(DIMENSION_LISTS);
        taText.setBackground(COLOR_BACKGROUND_DISABLE_COMPONENTS);
        taText.setForeground(COLOR_TEXT_DISABLE_COMPONENTS);
        taText.setLineWrap(true);
        layerText.add(lblText);
        layerText.add(taText);
        layerText.setBackground(null);

        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });

        btnSubmit = new JButton("Guardar");
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnSubmit(event);
            }
        });

        //Adding the Components on the panels
        westListPanel.add(lstCandidatures);
        westDecisionPanel.add(layerRadioButtons);
        westDecisionPanel.add(layerText);

        westPanel.add(westListPanel);
        westPanel.add(westDecisionPanel);

        eastPanel.add(layerName);
        eastPanel.add(layerAddress);
        eastPanel.add(layerCellPhoneNumber);
        eastPanel.add(layerArea);
        eastPanel.add(layerInvitaitions);
        eastPanel.add(layerProducts);
        eastPanel.add(layerDemonstracao);

        centerPanel.add(westPanel, BorderLayout.WEST);
        centerPanel.add(eastPanel, BorderLayout.EAST);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnSubmit);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Generate the decision
     *
     * @param event
     */
    private void clickBtnSubmit(ActionEvent event) {
        try {
            String text = taText.getText();
            if (text.isEmpty()) {
                throw new IllegalArgumentException("Texto Justificativo em falta.");
            }
            if (lstCandidatures.isSelectionEmpty()) {
                throw new IllegalArgumentException("Deverá selecionar uma candidatura.");
            }
            boolean accepted = radioGroup.isSelected(rbAccept.getModel());
            ctrl.createDecisao(accepted, text);
            Candidatura c = lstCandidatures.getSelectedValue();
            lstCandidatures.clearSelection();
            ctrl.registarDecisao(c);
            refreshCandidature(null);
        } catch (IllegalArgumentException e) {
        }
    }

    /**
     * Set the uc to default
     */
    private void defaultForm() {
        radioGroup.setSelected(rbAccept.getModel(), true);
        taText.setText("");

        tfInputName.setText("");
        tfInputAddress.setText("");
        tfInputCellPhoneNumber.setText("");
    }

    /**
     * Cancela o caso de uso.
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        lstCandidatures.clearSelection();
        lstCandidatures.setModel(new DefaultListModel<>());
        defaultForm();
    }

    /**
     * Update the form of candidature
     *
     * @param c candidature that was selected
     */
    private void refreshCandidature(Candidatura c) {
        if (lstCandidatures.getModel().getSize() == 0) {
            throw new IllegalAccessCandidaturasPorAtribuir("De momento, não existem candidaturas por decidir para o evento selecionado");
        }
        if (c != null) {
            tfInputName.setText(c.getNome());
            tfInputName.setToolTipText(c.getNome());

            tfInputAddress.setText(c.getMorada());
            tfInputAddress.setToolTipText(c.getMorada());

            tfInputCellPhoneNumber.setText(Integer.toString(c.getTelemovel()));
            tfInputCellPhoneNumber.setToolTipText(Integer.toString(c.getTelemovel()));
        }
    }

    /**
     * Method that starts the use case
     */
    public void startingUseCase() {
        try {
            this.ctrl = new DecidirCandidaturaController(cde);

            new DialogoEscolhaEventosGUI(mainFrame, "Escolher Evento", true, (Controller) this.ctrl);

            if (ctrl.hasEvento()) { //False when the user cancel the uc on popup window
                ctrl.obterCandidaturas();

                lstCandidatures.setModel(new AbstractListModel<Candidatura>() {

                    List<Candidatura> list = ctrl.getListaCandidaturasFAE();
                    @Override
                    public int getSize() {
                        return list.size();
                    }

                    @Override
                    public Candidatura getElementAt(int i) {
                        return list.get(i);
                    }
                });

                lstCandidatures.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent lse) {
                        Candidatura c = lstCandidatures.getSelectedValue();
                        refreshCandidature(c);
                    }
                });
            }
        } catch (IllegalAccessError ex) {
            JOptionPane.showMessageDialog(mainFrame, ex.getMessage(), "Informação", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
