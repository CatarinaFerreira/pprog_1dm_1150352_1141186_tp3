/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.AtribuirCandidaturaController;
import Models.AlgoritmoAtribuicao;
import Models.Atribuicao;
import Models.CentroEventos;
import Models.Data;
import Models.Evento;
import UI.AtribuirCandidaturaUI;
import UI.TabChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class AtribuirCandidaturaGUI extends JPanel implements TabChangeListener {

    /*
     Sempre que for necessário chamar um método do controller, usar    aaUI.nome_do_metodo();  
     */
    private final CentroEventos centroEventos;

    private final AtribuirCandidaturaUI aaUI;
    /**
     * Variável de ligação à classe Atribuir Candidatura Controller.
     */

    private DefaultListModel<Evento> eventoModel = new DefaultListModel<>();

    private JList<Evento> eventoList;

    private JList<AlgoritmoAtribuicao> algList;

    private DefaultListModel<AlgoritmoAtribuicao> algModel = new DefaultListModel<>();

    private JList<Atribuicao> atrList;

    private DefaultListModel<Atribuicao> atrModel = new DefaultListModel<>();

    private Data data = Data.dataAtual();

    public AtribuirCandidaturaGUI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
        this.aaUI = new AtribuirCandidaturaController(centroEventos);

        setLayout(new BorderLayout());

        construirGUI();
    }

    private void displayError(String message) {
        JOptionPane.showMessageDialog(this, message, "ERRO", JOptionPane.ERROR_MESSAGE);
    }

    private void construirGUI() {
        setLayout(new GridLayout(4, 1));
        adicionarListaEventos();
        adicionarAlgoritmo();
        adicionarAtribuicao();
        adicionarRegistarAtribuicoesGUI();
    }

    private void adicionarRegistarAtribuicoesGUI() {
        JPanel mainPanel = new JPanel();
        JButton registerButton = new JButton();
        registerButton.setText("Registar Atribuições");

        registerButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (aaUI.validaAtribuicoes()) {
                    aaUI.addListaAtribuicoes();
                    JOptionPane.showMessageDialog(null, "Registo efetuado com sucesso!");
                    construirGUI();
                } else {
                    displayError("Erro - atribuições já existentes.");
                }
            }
        });

        mainPanel.add(registerButton);

        add(mainPanel);
    }

    private void adicionarAtribuicao() {
        atrList = new JList();
        atrModel = new DefaultListModel();
        atrList.setModel(atrModel);
        JScrollPane pane = new JScrollPane(atrList);

        pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.lightGray, Color.lightGray), "Atribuições"));
        add(pane);
    }

    private void adicionarAlgoritmo() {
        JPanel mainPanel = new JPanel();
        algList = new JList();
        algModel = new DefaultListModel();
        algList.setModel(algModel);
        JScrollPane pane = new JScrollPane(algList);

        pane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY, 2, true), "Algoritmos de Atribuição"));
        add(pane);
        algList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    atrModel.clear();
                    aaUI.getListaCandidaturas();
                    aaUI.getListaFAE();
                    aaUI.selectAlgoritmo(algList.getSelectedValue());
                    List<Atribuicao> list = aaUI.createListaAtribuicoes();
                    if (!list.isEmpty()) {
                        for (Atribuicao element : list) {
                            atrModel.addElement(element);
                        }
                    } else {
                        displayError("Não existem atribuições para o algoritmo e evento selecionados.");
                    }
                }
            }
        });
        //add(mainPanel);
    }

    private void adicionarListaEventos() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2, 1));

        JPanel selectPanel = new JPanel();
        eventoList = new JList();
        eventoList.setModel(eventoModel);
        eventoList.setMinimumSize(new Dimension(256, 256));
        eventoList.setPreferredSize(new Dimension(256, 256));
        JScrollPane pane = new JScrollPane(eventoList);

        pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY, 2, true), "Eventos"));
        JButton selectButton = new JButton("Selecionar Evento");
        selectButton.addActionListener(new ActionListener() {

            /**
             * Ao pressionar no button, é obrigatório apresentar a lista de
             * AlgoritmoAtribuicao Apresentar as listas de Candidatura e FAE é
             * opcional
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (eventoList.getSelectedValue() != null) {
                    aaUI.selectEvento(eventoList.getSelectedValue());
                    List<AlgoritmoAtribuicao> list = aaUI.getListaAlgoritmos();
                    if (!list.isEmpty()) {
                        for (AlgoritmoAtribuicao element : list) {
                            algModel.addElement(element);
                        }
                    } else {
                        displayError("Não existem algoritmos de atribuição.");
                    }
                }
            }
        });
        selectPanel.add(selectButton);

        mainPanel.add(pane);
        mainPanel.add(selectPanel);
        add(mainPanel);
    }

    @Override
    public void onTabChanged() {
        removeAll();
        construirGUI();
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                List<Evento> list = aaUI.getListaEventosOrganizadorAtribuicao(centroEventos.getLoggedInUser(), data);
                if (!list.isEmpty()) {
                    for (Evento element : list) {
                        eventoModel.addElement(element);
                    }
                } else {
                    displayError("Não existe nenhum evento.");
                }
            }
        });
    }
}
