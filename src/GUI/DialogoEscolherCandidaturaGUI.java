/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import Controllers.DecidirCandidaturaController;
import Models.Candidatura;
import Models.CentroEventos;
import Models.Evento;
import Models.FAE;

public class DialogoEscolherCandidaturaGUI extends JDialog {
    
    private final JFrame framePai;
    private Evento evento;
    private FAE fae;
    private CentroEventos centroEventos;
    private DecidirCandidaturaController deci_controller;
    private JComboBox comboCandidaturas;
    private static final Dimension DIMENSION = new Dimension(1000, 300);
    
    public DialogoEscolherCandidaturaGUI(JFrame framePai, CentroEventos centroEventos, FAE fae, Evento evento) {

        super(framePai, "Escolha de Candidatura", true);
        this.framePai = framePai;
        this.centroEventos = centroEventos;
        this.fae = fae;
        this.evento = evento;
//        this.deci_controller = new DecidirCandidaturaController(centroEventos, evento);

        setLayout(new BorderLayout());
        setPreferredSize(DIMENSION);

        criarComponentes();
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public void criarComponentes() {
        JPanel p1 = criarPainelCandidatura();
        JPanel p2 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);
    }

    private JPanel criarPainelCandidatura() {
        JPanel p = new JPanel();
        p.add(criarComboBoxCandidaturas());
        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarBotaoGuardar());
        p.add(criarBotaoCancelar());
        return p;
    }

    private JComboBox criarComboBoxCandidaturas() {
        comboCandidaturas = new JComboBox(deci_controller.getListaCandidaturasFAE().toArray());
        comboCandidaturas.setSelectedIndex(-1);
        return comboCandidaturas;
    }

    private JButton criarBotaoGuardar() {
        JButton btn = new JButton("Guardar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Candidatura c = (Candidatura) comboCandidaturas.getSelectedItem();

                deci_controller.setCandidatura(c);
                dispose();
                new DialogoDecidirCandidaturaGUI(framePai, deci_controller);
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
