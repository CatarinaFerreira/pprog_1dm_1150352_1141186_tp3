/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.DecidirCandidaturaController;
import Models.CentroEventos;
import UI.DecidirCandidaturaUI;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DialogoDecidirCandidaturaGUI extends JDialog{
    
    private CentroEventos centroEventos;
    private DecidirCandidaturaUI dcUI;
 
    private static final String[] avaliacao = {"Aprovada", "Rejeitada"};
    private DecidirCandidaturaController controller;
    private JTextArea txtJustificacao;
    private JComboBox comboBox;
    private Dimension TAMANHO = (new Dimension(500, 150));

    public DialogoDecidirCandidaturaGUI(JFrame framePai, DecidirCandidaturaController controller) {
        super(framePai, "Decisão da Candidatura");
        this.controller = controller;
        this.dcUI = controller;
        // this.dcUI = new DecidirCandidaturaController(centroEventos);
        criarComponentes();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes() {
        JPanel p1 = criarPainelAvaliação();
        JPanel p2 = criarPainelTexto();
        JPanel p3 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);
    }

    private JPanel criarPainelAvaliação() {
        JPanel p = new JPanel();
        JLabel lbl = new JLabel("Decisão");
        comboBox = new JComboBox(avaliacao);
        comboBox.setSelectedIndex(-1);
        p.add(lbl);
        p.add(comboBox);
        return p;
    }

    private JPanel criarPainelTexto() {
        JPanel p = new JPanel();

        JLabel lbl = new JLabel("Texto Justificativo");
        txtJustificacao = new JTextArea();
        txtJustificacao.setPreferredSize(TAMANHO);

        p.add(lbl);
        p.add(txtJustificacao);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarBotaoSubmeter());
        p.add(criarBotaoCancelar());
        return p;
    }

    private JButton criarBotaoSubmeter() {
        JButton btn = new JButton("Submeter");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int i = comboBox.getSelectedIndex();
                /* usar o if abaixo, este será para apagar
                if (i == 0) {
                    controller.setDecisao("Aprovada");
                } else {
                    controller.setDecisao("Rejeitada");
                }
                controller.setJustificao(txtJustificacao.getText());
                */
                if (i == 0) {
                    dcUI.createDecisao(true, txtJustificacao.getText());
                } else {
                    dcUI.createDecisao(false, txtJustificacao.getText());
                }
                        
                controller.addDecisao();
                JOptionPane.showMessageDialog(null,"Candidatura decidida com sucesso!",
                        "",JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }

        });
        return btn;
    }   
}
