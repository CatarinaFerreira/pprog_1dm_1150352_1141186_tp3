/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.Controller;
import Models.Evento;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * JDialog that serves to choose an Evento
 */
public class DialogoEscolhaEventosGUI extends JDialog {

    //GLOBAL WINDOW COMPONENTS
    private JPanel topPanel;
    private JPanel centerPanel;
    private JPanel bottomPanel;
    private JList<Evento> listEventos;
    private JButton btnAccept;
    private JButton btnCancel;
    //GLOBAL CONSTANTS
    /**
     * Main frame that contains this panel
     */
    private final Frame mainFrame;
    /**
     * Controller of the use case
     */
    private final Controller ctrl;
    /**
     * Window default layout manager of contentPane
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout();

    /**
     * Constructor of the dialog window
     *
     * @param mainFrame Main frame
     * @param title title bar
     * @param type specifies whether dialog blocks user input to other top-level
     * windows when shown. If true, the modality type property is set to
     * DEFAULT_MODALITY_TYPE otherwise the dialog is modeless
     * @param ctrl Controller of the use case
     */
    public DialogoEscolhaEventosGUI(Frame mainFrame, String title, boolean type, Controller ctrl) {
        super(mainFrame, title, type);
        this.mainFrame = mainFrame;
        this.ctrl = ctrl;

        preSetupFrame();
        initializeComponents();
        posSetupFrame();
    }

    /**
     * Setup the ContentPane/window of dialog frame before the initialize the
     * components
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    /**
     * Setup the ContentPane/window of dialog frame after the initialize the
     * components
     */
    private void posSetupFrame() {
        getRootPane().setDefaultButton(btnAccept);
        pack();
        setMinimumSize(getSize());
        setResizable(false);
        setLocationRelativeTo(mainFrame);
        setVisible(true);
    }

    private void initializeComponents() {
        //Containers of components
        centerPanel = new JPanel();

        bottomPanel = new JPanel();

        //Components and Layers
        listEventos = new JList<>();
        listEventos.setModel(new AbstractListModel<Evento>() {
            List<Evento> list = ctrl.listarEventos();

            @Override
            public int getSize() {
                return list.size();
            }

            @Override
            public Evento getElementAt(int i) {
                return list.get(i);
            }
        });
        listEventos.setBackground(null);
        listEventos.setBorder(new TitledBorder("Eventos"));

        btnAccept = new JButton("Aceitar");
        btnAccept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnAccept(event);
            }
        });
        btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                clickBtnCancel(event);
            }
        });
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                dispose();
            }

        });

        //Adding the Components on the panels
        centerPanel.add(listEventos);

        bottomPanel.add(btnCancel);
        bottomPanel.add(btnAccept);

        //Adding the panels/Components on the content pane
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * submit a product
     *
     * @param event
     */
    private void clickBtnAccept(ActionEvent event) {
        try {
            Evento e = listEventos.getSelectedValue();
            ctrl.definirEventos(e);
            dispose();
        } catch (IllegalAccessError ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Close the popup window
     *
     * @param event
     */
    private void clickBtnCancel(ActionEvent event) {
        dispose();
    }
}