/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Lists.ListaCandidaturas;
import Models.CentroEventos;
import Models.Evento;
import Models.FicheiroCentroEventos;
import Models.Utilizador;
import UI.TabChangeListener;
import Utils.Descodificador;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

public class JanelaMenu extends JFrame {

    /**
     * Painéis a serem adicionados.
     */
    private JPanel painelMenu, painelSup;
    
    /**
     * Window default width size
     */
    private final int WINDOW_DEFAULT_WIDTH = 900;
    /**
     * Window default height size
     */
    private final int WINDOW_DEFAULT_HEIGHT = 600;
    /**
     * Window default layout manager of contentPane
     */
    private final LayoutManager WINDOW_DEFAULT_LAYOUT = new BorderLayout();
    /**
     * Window default if is set visible or not
     */
    private final boolean WINDOW_DEFAULT_IS_VISIBLE = true;
    /**
     * Window default background color
     */
    public static final Color WINDOW_DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    /**
     * Item para exportar o ficheiro.
     */
    private JMenuItem exportar;
    private JFileChooser fileChooser;
    private FicheiroCentroEventos ficheiroCentroEventos;
    private Utilizador user;
    private JTabbedPane tabPane;
    private JList lstCompleta;
    private PainelListaCandidaturasGUI pListaCandidaturas;
    private ListaCandidaturas listaCandidaturas;
    private CentroEventos centroEventos;
    private Evento evento;

    /**
     * Constrói a Janela que irá conter diversos painéis com diversas
     * funcionalidades para o correto funcionamento da aplicação.
     *
     * @param centroeventos o centro de eventos.
     * @param ficheiroCentroEventos o ficheiro do centro de eventos.
     */
    public JanelaMenu(CentroEventos centroeventos, FicheiroCentroEventos ficheiroCentroEventos) {
        super("INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO (ISEP)");

        painelSup = criarPainelSuperior();
        add(painelSup, BorderLayout.NORTH);
        painelMenu = criarPainelMenu();
        add(painelMenu, BorderLayout.CENTER);
        tabPane = criarSeparadores();
        add(tabPane, BorderLayout.CENTER);
        this.centroEventos = centroeventos;
        preSetupFrame();
        posSetupFrame(WINDOW_DEFAULT_IS_VISIBLE);
        criarComponentes();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            }
        });

        tabPane.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                if (tabPane.getSelectedComponent() instanceof TabChangeListener) {
                    ((TabChangeListener) tabPane.getSelectedComponent()).onTabChanged();
                }
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setSize(WINDOW_DEFAULT_WIDTH, WINDOW_DEFAULT_HEIGHT);
        
        setLocationRelativeTo(null);

        setVisible(true);
    }

     /**
     * Setup the ContentPane/window of MainFrame before the initialize the
     * components
     */
    private void preSetupFrame() {
        setLayout(WINDOW_DEFAULT_LAYOUT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                dispose();
            }
        });
    }
    
    /**
     * Setup the ContentPane/window of MainFrame after the initialize the
     * components
     *
     * @param visible Set if the window will be visible
     */
    private void posSetupFrame(boolean visible) {
        pack();
        setMinimumSize(getSize());
        setLocationRelativeTo(null);
        setVisible(visible);
        //Login
        new IniciarSessaoGUI(this, "Selecionar Utilizador", true, centroEventos);
    }
    
    /**
     * Constrói o painel que contém os botões dos casos de uso.
     *
     * @return painel.
     */
    public JPanel criarPainelMenu() {
        JPanel painel = new JPanel(new GridLayout(4, 2, 30, 20));

        final int MARGEM_SUPERIOR = 30, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 40, MARGEM_DIREITA = 40;

        painel.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        JButton botao1 = new JButton("Atribuir Candidatura");
//        botao1.add();
        painel.add(botao1);

        JButton botao2 = new JButton("Decidir Candidatura");
        botao2.add(criarItemDecidirCandidatura());
        painel.add(botao2);

        JButton botao3 = new JButton("Submeter Candidatura");
        botao3.add(criarItemNovaCandidatura());
        painel.add(botao3);

        return painel;
    }

     /**
     * Constrói painel que irá ser colocado na parte superior do painel contendo o nome da instituição de ensino para 
     * a qual a avaliação está a ser efetuada.
     * @return painel.
     */
    
    public JPanel criarPainelSuperior() {
        JPanel painel = new JPanel();
        
        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        
        painel.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));
        
        JLabel lbl = new JLabel();
        
        lbl.setText("ORGANIZAÇÃO E GESTÃO DE EVENTOS");
        painel.add(lbl, BorderLayout.CENTER);
        
        JLabel JLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        return painel;
    }

    /**
     * Cria e adiciona componentes à janela.
     */
    private void criarComponentes() {
        JMenuBar menu = criarBarraMenus();
        setJMenuBar(menu);

        tabPane = criarSeparadores();
        add(tabPane, BorderLayout.CENTER);
    }

    /**
     * Cria, na barra superior, um menu que irá conter um outro menu com
     * diversas opções.
     *
     * @return MenuBar opções.
     */
    private JMenuBar criarBarraMenus() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenuOpcoes());

        return menuBar;
    }

    /**
     * Cria o menu Opções que irá conter várias opções de escolha para o
     * utilizador.
     *
     * @return
     */
    private JMenu criarMenuOpcoes() {
        JMenu menu1 = new JMenu("Opções");
        menu1.setMnemonic(KeyEvent.VK_O);

        menu1.addSeparator();
        menu1.add(criarMenuLista());
        menu1.addSeparator();
        menu1.add(criarMenuPrincipal());
        menu1.addSeparator();
        menu1.add(criarItemSair());

        return menu1;
    }

    /**
     * Constrói a componente do menu Opções que apresenta o menu Lista que, por
     * sua vez, apresenta diversas opções.
     *
     * @return Lista lista.
     */
    private JMenu criarMenuLista() {
        JMenu menu2 = new JMenu("Lista");
        menu2.add(criarSubMenuLista());
        menu2.add(criarItemExportar());

        return menu2;
    }

    /**
     * Constrói o menu MenuPrincipal que irá conter 3 opções de escolha para o
     * utilizador da aplicação.
     *
     * @return
     */
    private JMenu criarMenuPrincipal() {
        JMenu menu3 = new JMenu("Menu Principal");
        menu3.add(criarSubMenuPrincipal1());
        menu3.add(criarSubMenuPrincipal2());
        menu3.add(criarSubMenuPrincipal3());

        return menu3;
    }

    /**
     * Constrói a componente do submenu Lista que irá ter as opções de
     * importação em Importar.
     *
     * @return
     */
    private JMenu criarSubMenuLista() {
        JMenu menu4 = new JMenu("Importar");
        menu4.setMnemonic(KeyEvent.VK_I);

        menu4.add(criarItemImportarBinario());
        menu4.add(criarItemImportarTexto());

        fileChooser = new JFileChooser();

        return menu4;
    }

    /**
     * Constrói um submenu que irá conter o item para realizar o UC3 (caso de
     * uso 3).
     *
     * @return
     */
    private JMenuItem criarSubMenuPrincipal1() {
        JMenuItem item = new JMenuItem("Atribuir Candidatura", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
            item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                criarPainelAviso(); 
            }    
            });
        return item;
    }
    
    private JPanel criarPainelAviso() {
        JLabel lbl = new JLabel("Por favor, selecione o separador que contém o título 'Atribuir Candidaturas'", JLabel.CENTER);

        final int CAMPO_LARGURA = 20;

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);

        return p;  
    }

    /**
     * Constrói um submenu que irá conter o item para realizar o UC4 (caso de uso 4).
     *
     * @return
     */
    private JMenuItem criarSubMenuPrincipal2() {
        JMenuItem item = new JMenuItem("Decidir Candidatura", KeyEvent.VK_C);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Evento ev = centroEventos.getListaEventos().get(0);
                DialogoEscolherCandidaturaGUI dialogoExibirCandidatura = new DialogoEscolherCandidaturaGUI
                    (JanelaMenu.this, centroEventos, ev.getListaFAE().get(0), ev);
            }
        });

        return item;
    }

    /**
     * Constrói um submenu que irá conter o item para realizar o UC5 (caso de
     * uso 5).
     *
     * @return
     */
    private JMenuItem criarSubMenuPrincipal3() {
        JMenuItem item = new JMenuItem("Submeter Candidatura", KeyEvent.VK_R);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
         item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogoSubmeterCandidaturaGUI dialogoNovaCandidatura = new DialogoSubmeterCandidaturaGUI(JanelaMenu.this, listaCandidaturas, tabPane, pListaCandidaturas,centroEventos);
            }
        });

        return item;
    }

    /**
     * Constrói a componente do menu Importar que permite ler um ficheiro
     * binário e instanciar classes no centro de eventos.
     *
     * @return Opção importar ficheiro de texto.
     */
    private JMenuItem criarItemImportarBinario() {
        JMenuItem item1 = new JMenuItem("Ficheiro Binário", KeyEvent.VK_B);
        item1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
        item1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                definirFiltroExtensaoBin(fileChooser);

                int resposta = fileChooser.showOpenDialog(JanelaMenu.this);

                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    CentroEventos centroEventos = ficheiroCentroEventos.ler(file.getPath());
                    if (centroEventos == null) {
                        JOptionPane.showMessageDialog(
                                JanelaMenu.this,
                                "Impossível ler o ficheiro: " + file.getPath() + " !",
                                "Importar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        dispose();
                        JOptionPane.showMessageDialog(
                                JanelaMenu.this,
                                "Importado com sucesso do ficheiro!",
                                "Importar Centro Eventos",
                                JOptionPane.INFORMATION_MESSAGE);
                        new JanelaMenu(centroEventos, ficheiroCentroEventos);
                    }
                }
            }
        });

        return item1;
    }

    /**
     * Constrói a componente do menu Importar que permite ler um ficheiro de
     * texto e instanciar classes no centro de eventos.
     *
     * @return Opção importar ficheiro de texto.
     */
    private JMenuItem criarItemImportarTexto() {
        JMenuItem item = new JMenuItem("Ficheiro Texto", KeyEvent.VK_T);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                definirFiltroExtensaoTxt(fileChooser);

                int resposta = fileChooser.showOpenDialog(JanelaMenu.this);

//                if (resposta == JFileChooser.APPROVE_OPTION) {
//                    try {
//                        File file = fileChooser.getSelectedFile();
//                       // int count=Descodificador.lerFicheiro(file, centroEventos);
//                       int count = Descodificador.lerFicheiro_Utilizadores(centroEventos);
//                       
//                        if (count==0) {
//                            JOptionPane.showMessageDialog(
//                                    JanelaMenu.this,
//                                    "NÃ£o foram instanciadas quaisquer classes!",
//                                    "Instanciar classes",
//                                    JOptionPane.ERROR_MESSAGE);
//                        } else {
//                            dispose();
//                            JOptionPane.showMessageDialog(
//                                    JanelaMenu.this,
//                                    "Foram instanciadas "+count+" classes.",
//                                    "Instanciar classes",
//                                    JOptionPane.INFORMATION_MESSAGE);
//                            new JanelaMenu(centroEventos, ficheiroCentroEventos);
//                        }
//                    } catch (FileNotFoundException ex) {
//                        Logger.getLogger(JanelaMenu.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
            }
        });

        return item;
    }

    /**
     * Define a extensão .txt para ler de um ficheiro de texto.
     *
     * @param fileChooser.
     */
    private void definirFiltroExtensaoTxt(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("txt");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.txt";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    /**
     * Define a extensão .bin para ler de um ficheiro binário.
     *
     * @param fileChooser.
     */
    private void definirFiltroExtensaoBin(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("bin");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.bin";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    /**
     * Constrói a opção no menu da barra superior que permite exportar o estado
     * atual do centro de eventos para um ficheiro binário.
     *
     * @return Opção exportar.
     */
    private JMenuItem criarItemExportar() {
        JMenuItem item = new JMenuItem("Exportar", KeyEvent.VK_E);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                definirFiltroExtensaoBin(fileChooser);

                int resposta = fileChooser.showSaveDialog(JanelaMenu.this);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (!file.getName().endsWith(".bin")) {
                        file = new File(file.getPath().trim() + ".bin");
                    }
                    boolean ficheiroGuardado = false;
                    try {
                        ficheiroGuardado = ficheiroCentroEventos.guardar(file.getPath(), centroEventos);
                    } catch (IOException ex) {
                        Logger.getLogger(JanelaMenu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (!ficheiroGuardado) {
                        JOptionPane.showMessageDialog(
                                JanelaMenu.this,
                                "Impossível gravar o ficheiro: "
                                + file.getPath() + " !",
                                "Exportar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                JanelaMenu.this,
                                "Ficheiro gravado com sucesso.",
                                "Exportar",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }

    /**
     * Constrói o item que irá fazer com que seja visível uma janela para o
     * registo de uma nova candidatura.
     *
     * @return
     */
    private JMenuItem criarItemNovaCandidatura() {
        JMenuItem item = new JMenuItem("Submeter Candidatura", KeyEvent.VK_N);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogoSubmeterCandidaturaGUI dialogoNovaCandidatura = new DialogoSubmeterCandidaturaGUI(JanelaMenu.this, 
                        listaCandidaturas, tabPane, pListaCandidaturas,centroEventos);
            }
        });

        return item;
    }

    /**
     * Constrói o item que irá fazer com que seja visível a janela para a decisão de candidaturas.
     * @return 
     */
    
    private JMenuItem criarItemDecidirCandidatura() {
        JMenuItem item = new JMenuItem("Decidir Candidatura", KeyEvent.VK_D);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Evento evento = centroEventos.getListaEventos().get(0); // ???
                DialogoEscolherCandidaturaGUI dialogoExibirCandidatura = new DialogoEscolherCandidaturaGUI
                        (JanelaMenu.this, centroEventos, evento.getListaFAE().get(0), evento);
            }
        });

        return item;
    }

    /**
     * Constrói os separadores da página inicial da Interface.
     *
     * @return
     */
    private JTabbedPane criarSeparadores() {
        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("Início", new PaginaInicial());

        pListaCandidaturas = new PainelListaCandidaturasGUI(evento);
        tabPane.addTab("Candidaturas Registadas", pListaCandidaturas);
        
        tabPane.addTab("Atribuir Candidaturas",new AtribuirCandidaturaGUI(centroEventos));
        
        tabPane.addTab("Decisão Candidaturas", new PainelListaDecisoesGUI(centroEventos));
        return tabPane;
    }

    /**
     * Cria, na barra superior, a opção do menu que permite sair da aplicação.
     *
     * @return Opção sair.
     */
    private JMenuItem criarItemSair() {
        JMenuItem item3 = new JMenuItem("Sair", KeyEvent.VK_S);
        item3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean confirmacao = JOptionPane.showConfirmDialog(null,
                "Pretende sair do programa?",
                "Confirmar",
                JOptionPane.OK_OPTION) == 0;
                if (confirmacao) {
                    Descodificador.writeObject(Descodificador.NAME_OF_DEFAULT_FILE_DATA, centroEventos);
                    System.exit(0);
        }
                dispose();
            }
        });

        return item3;
    }
}
