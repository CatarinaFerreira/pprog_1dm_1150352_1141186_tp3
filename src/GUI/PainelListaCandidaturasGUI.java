/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controllers.ListarCandidaturasController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import Models.Candidatura;
import Models.Evento;
import UI.TabChangeListener;

public class PainelListaCandidaturasGUI extends JPanel implements TabChangeListener {
    
    /**
     * Lista de candidaturas.
     */
    private JList<Candidatura> list; 
    
    private DefaultListModel<Candidatura> model;
    
    /**
     * Controller ListarCandidaturas.
     */
    private ListarCandidaturasController controller;
  
    /**
    * Constrói o painél que irá conter uma lista com todas as candidaturas. 
    * @param evento evento.
    */   
    public PainelListaCandidaturasGUI(Evento evento) {
        super();
        controller = new ListarCandidaturasController(evento);
        setLayout(new BorderLayout());
        
        add(criarPainelListaCandidaturas(), BorderLayout.CENTER);
    }

    /**
     * Método que atualiza a lista de candidaturas.
     */
    
    void atualizarListaCandidaturas(){
        model.clear();
        List<Candidatura> candidaturas = controller.getListaCandidaturas();
        for (Candidatura element:candidaturas) {
            model.addElement(element);
        }
    }
    
    /**
     * Constrói o painel com a lista de candidaturas.
     * @return 
     */
    private JScrollPane criarPainelListaCandidaturas() {

        model = new DefaultListModel();
        list = new JList();
        list.setModel(model);

        JScrollPane scrollPane = new JScrollPane(list);
        
        scrollPane.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createLineBorder(Color.GRAY,2,true), "Candidaturas Registadas"));
        
        return scrollPane;
    }

    /**
     * Método que permite que, quando o utilizador selecione a opção Guardar, o visor alterne automaticamente para
     * o separador Candidaturas Registadas, demonstrando as candidaturas.
     */   
    @Override
    public void onTabChanged() {
        atualizarListaCandidaturas();
    }  
}
