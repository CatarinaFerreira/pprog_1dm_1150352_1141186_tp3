/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Models.CentroEventos;
import Utils.Descodificador;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class DialogoEscolhaFicheiro extends JFileChooser  {
    
    /**
     * Construtor
     */
    public DialogoEscolhaFicheiro() {
        setup();
    }

    /**
     * Setup the JFileChooser
     */
    private void setup() {
        setMultiSelectionEnabled(false);
        setFileFilter(new FileNameExtensionFilter("Text Files (*.txt)", "txt"));
        setCurrentDirectory(new File(System.getProperty("user.dir")));
    }

    /**
     * when the file is selected, the jfilechooser handle with the text file and
     * write a object that was initialize from a method on FILEHANDLER, and
     * write in a binary file
     */
    public void saveFileInitializer() {
        File file = getSelectedFile();
        if (!file.getName().contains(".txt")) {
            throw new IllegalArgumentException("O Ficheiro deve ser de texto (*.txt)");
        }
        //read the config file (.txt)
        CentroEventos importedObject = Descodificador.importFileInitializer(file);
        //write data on .bin
        Descodificador.writeObject(Descodificador.NAME_OF_DEFAULT_FILE_DATA, importedObject);
    }   
}
